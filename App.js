import React, {useEffect, useState} from 'react';
// import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import UserStore from './src/contexts/stores/UserStore';
import Base from './src/components/navigation/Base';

import {fcmService} from './src/FCMService';
import {localNotificationService} from './src/LocalNotificationService';
import PushNotification, {Importance} from 'react-native-push-notification';

const App = ({navigation}) => {
  const backgroundStyle = {
    backgroundColor: Colors.lighter,
    minHeight: '100%',
  };

  const [deviceId, setDeviceId] = useState('');

  useEffect(() => {
    fcmService.registerAppWithFCM();
    fcmService.register(onRegister, onNotification, onOpenNotification);
    localNotificationService.configure(onOpenNotification);

    function onRegister(token) {
      console.log('[App] onRegister: ', token, 'first .....................');
      setDeviceId(token);
    }

    function onNotification(notify) {
      console.log(
        '[App] onNotification: ',
        notify,
        '2------------------------------',
      );
      const options = {
        soundName: 'default',
        playSound: true, //,
        largeIcon: 'ic_notification', // add icon large for Android (Link: app/src/main/mipmap)
        smallIcon: 'ic_notification', // add icon small for Android (Link: app/src/main/mipmap)
      };
      localNotificationService.showNotification(
        0,
        notify.title,
        notify.body,
        notify,
        options,
      );
    }
    function onOpenNotification(notify) {
      console.log('[App] onOpenNotification: ', notify);
      // alert('Open Notification: ' + notify.body);
    }

    PushNotification.createChannel(
      {
        channelId: 'channel-id', // (required)
        channelName: 'My channel', // (required)
        channelDescription: 'A channel to categorise your notifications', // (optional) default: undefined.
        playSound: true, // (optional) default: true
        soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
        importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
      },
      created =>
        console.log(
          `createChannel returned '${created}'`,
          'third .....................',
        ), // (optional) callback returns whether the channel was created, false means it already existed.
    );

    PushNotification.getChannels(function (channel_ids) {
      console.log(channel_ids, 'channel id', 'fourth .....................'); // ['channel_id_1']
    });

    return () => {
      console.log('[App] unRegister', 'fifth .....................');
      fcmService.unRegister();
      localNotificationService.unregister();
    };
  }, []);

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar backgroundColor={'#fff'} barStyle="dark-content" />
      <UserStore>
        <Base deviceId={deviceId} />
      </UserStore>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
