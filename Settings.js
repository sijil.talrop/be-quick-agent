// export const BASE_URL = 'http://192.168.43.43:8001/api/v1/';
// export const BASE_URL = 'http://c7c56db241ff.ngrok.io/api/v1/';
export const BASE_URL = 'https://bequick-app.talrop.works/api/v1/';
// export const BASE_URL = 'https://bqmart.in/api/v1/';

export const THEME_COLOR = '#E3021C';
export const StatusBar_COLOR = '#b10216';
