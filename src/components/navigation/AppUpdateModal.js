import React, {
  useState,
  useEffect,
  useRef,
  useCallback,
  useContext,
} from 'react';
import {
  ScrollView,
  View,
  Image,
  StyleSheet,
  Text,
  Dimensions,
  SafeAreaView,
  TouchableOpacity,
  Keyboard,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
const {width, height} = Dimensions.get('window');
const card_height = 110;
const Reward_Box_Height = width * 0.2;
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

import LottieView from 'lottie-react-native';

export default function AppUpdateModal(props) {
  const [loading, setLoading] = useState(false);

  return (
    <SafeAreaView>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.container}>
        <View style={styles.line} />
        <View style={{marginVertical: 30}}>
          <LottieView
            source={require('../../assets/lotties/update.json')}
            autoPlay
            style={{height: 100, width: 100}}
            loop
          />
        </View>

        <Text style={styles.text}>Updates Available</Text>
        <Text style={styles.sub}>Version {props.latestVersion}</Text>
        <TouchableOpacity activeOpacity={0.7} style={styles.button}>
          <Text
            style={{
              fontFamily: 'Poppins-SemiBold',
              paddingVertical: 10,
              // fontSize: 15,
              fontSize: RFValue(15, height),
              color: '#fff',
              textAlign: 'center',
            }}>
            Update Now
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 20,
  },
  line: {
    backgroundColor: '#f7f7f7',
    width: '30%',
    height: 5,
    alignSelf: 'center',
    borderRadius: 20,
  },
  text: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
    textAlign: 'center',
    color: '#000',
  },
  sub: {
    fontFamily: 'Poppins-Medium',
    fontSize: RFValue(13, height),
    color: '#ccc',
    marginTop: 5,
    textAlign: 'center',
  },
  button: {
    width: '100%',
    backgroundColor: '#8ec641',
    borderRadius: 10,
    alignSelf: 'center',
    marginTop: 30,
    marginBottom: 10,
    alignItems: 'center',
  },
});
