import 'react-native-gesture-handler';
import React, {useState, useEffect, useRef, useContext} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {View, Text, StatusBar} from 'react-native';
import OnDutyPage from '../screens/duty-option/OnDutyPage';
import DutyOptionPage from '../screens/duty-option/DutyOptionPage';
import LoginPage from '../screens/Login/LoginPage';
import ForgetPassword from '../screens/Login/ForgetPassword';
import ForgetVerification from '../screens/Login/ForgetVerification';
import Menu from '../screens/menu/Menu';
import DashBoardPage from '../screens/dash-board/DashBoardPage';
import NotificationPage from '../screens/dash-board/NotificationPage';
import OrdersTab from '../screens/order/OrdersTab';
import OrderInfoModal from '../screens/order/OrderInfoModal';
import Profile from '../screens/profile/Profile';
import Help from '../screens/Help/Help';
import PickUpPage from '../screens/pick-up/PickUpPage';
import OrderItem from '../screens/duty-option/OrderItem';
import SuccessPopup from '../screens/pick-up/SuccessPopup';
import IntroComponent from '../screens/intro-page/IntroComponent';
import AppUpdateModal from './AppUpdateModal';
import ForceUpdateModal from './ForceUpdateModal';
import {BASE_URL} from '../../../Settings';

import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Modal from 'react-native-modal';
import VersionNumber from 'react-native-version-number';

import {UserContext} from '../../contexts/stores/UserStore';

const Stack = createStackNavigator();

const Base = ({deviceId}) => {
  const {userState, userDispatch} = useContext(UserContext);

  const [latestVersion, setLatestVersion] = useState();
  const [forceUpgrade, setForceUpgrade] = useState();
  const [recommendedUpgrade, setRecommendedUpgrade] = useState();

  useEffect(() => {
    get_profile();
  }, []);

  useEffect(() => {
    if (deviceId === '') {
      console.log('id is empty');
    } else {
      console.log('id is available');
      userDispatch({
        type: 'UPDATE_USER',
        user: {
          device_id: deviceId,
        },
      });
    }
  }, [deviceId]);

  const get_profile = async () => {
    let user = await AsyncStorage.getItem('user');
    if (user) {
      let user_instance = JSON.parse(user);
      let access_token = user_instance['access_token'];
      let get_url = BASE_URL + 'delivery-agents/profile/';
      console.warn(get_url);
      axios
        .get(get_url, {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        })
        .then(response => {
          if (response.data.StatusCode == 6000) {
            let data = response.data.data;
            console.log(data);
            setTimeout(() => {
              userDispatch({
                type: 'UPDATE_USER',
                user: {
                  user: 'user',
                  is_duty: data.is_duty,
                  live_time: data.live_duty_time,
                },
              });
            }, 1500);
          } else {
            console.log('not user');
            setTimeout(() => {
              userDispatch({
                type: 'UPDATE_USER',
                user: {
                  user: 'not user',
                },
              });
            }, 1500);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      setTimeout(() => {
        userDispatch({
          type: 'UPDATE_USER',
          user: {
            user: 'not user',
          },
        });
      }, 1500);
    }
  };

  useEffect(() => {
    const getUpdate = () => {
      let get_url = BASE_URL + 'general/deliver-app-update/';
      axios
        .get(get_url, {})
        .then(response => {
          if (response.data.StatusCode === 6000) {
            setTimeout(() => {
              setLatestVersion(response.data.data.app_version);
              if (
                response.data.data.app_version > VersionNumber.appVersion &&
                response.data.data.recommended_upgrade
              ) {
                setRecommendedUpgrade(true);
              } else if (
                response.data.data.app_version > VersionNumber.appVersion &&
                response.data.data.force_upgrade
              ) {
                setForceUpgrade(true);
              } else {
                setRecommendedUpgrade(false);
                setForceUpgrade(false);
              }
            }, 2000);
          } else {
            setTimeout(() => {}, 2000);
          }
        })
        .catch(error => {
          console.log(error);
        });
    };
    getUpdate();
  }, []);

  const renderApp = () => {
    if (userState.user == '') {
      return <IntroComponent />;
    } else if (userState.user == 'user') {
      return <PickUpStackNavigator />;
    } else if (userState.user == 'not user') {
      return <AuthStackNavigator />;
    }
  };
  return (
    <>
      <NavigationContainer>
        {renderApp()}
        {/* recommended update modal */}

        <Modal
          onBackButtonPress={() => {
            setRecommendedUpgrade(false);
          }}
          onBackdropPress={() => {
            setRecommendedUpgrade(false);
          }}
          isVisible={recommendedUpgrade}
          backdropOpacity={0.4}
          style={{
            margin: 0,
            screenBackgroundColor: 'transparent',
            modalPresentationStyle: 'overCurrentContext',
          }}
          propagateSwipe={true}
          useNativeDriver={false}
          onSwipeComplete={() => setRecommendedUpgrade(false)}
          swipeDirection={['down']}
          animationInTiming={500}
          animationOutTiming={1500}>
          <View style={{flex: 1, justifyContent: 'flex-end'}}>
            <AppUpdateModal
              setRecommendedUpgrade={setRecommendedUpgrade}
              latestVersion={latestVersion}
            />
          </View>
        </Modal>

        {/* force update */}

        <Modal
          isVisible={forceUpgrade}
          backdropOpacity={0.4}
          style={{
            margin: 0,
            screenBackgroundColor: 'transparent',
            modalPresentationStyle: 'overCurrentContext',
          }}
          useNativeDriver={false}
          animationInTiming={500}
          animationOutTiming={1500}>
          <View style={{flex: 1, justifyContent: 'flex-end'}}>
            <ForceUpdateModal
              setForceUpgrade={setForceUpgrade}
              latestVersion={latestVersion}
            />
          </View>
        </Modal>
      </NavigationContainer>
    </>
  );
};

const PickUpStackNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="HomeStackNavigator"
      options={{
        headerShown: false,
      }}>
      <Stack.Screen
        name="PickUpPage"
        component={PickUpPage}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="HomeStackNavigator"
        component={HomeStackNavigator}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="SuccessPopup"
        component={SuccessPopup}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

const AuthStackNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="LoginPage"
      options={{
        headerShown: false,
      }}>
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name="LoginPage"
        component={LoginPage}
      />
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name="ForgetPassword"
        component={ForgetPassword}
      />
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name="ForgetVerification"
        component={ForgetVerification}
      />
    </Stack.Navigator>
  );
};

const HomeStackNavigator = () => {
  const {userState, userDispatch} = useContext(UserContext);
  return (
    <>
      <Stack.Navigator
        initialRouteName={
          userState.is_duty == false ? 'DutyOptionPage' : 'OnDutyPage'
        }
        options={{
          headerShown: false,
        }}>
        {userState.is_duty == false && (
          <Stack.Screen
            name="DutyOptionPage"
            component={DutyOptionPage}
            options={{
              headerShown: false,
            }}
          />
        )}
        {userState.is_duty == true && (
          <Stack.Screen
            name="OnDutyPage"
            component={OnDutyPage}
            options={{
              headerShown: false,
            }}
          />
        )}

        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="Menu"
          component={Menu}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="OrdersTab"
          component={OrdersTab}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="Help"
          component={Help}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="OrderInfoModal"
          component={OrderInfoModal}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="DashBoardPage"
          component={DashBoardPage}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="NotificationPage"
          component={NotificationPage}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="Profile"
          component={Profile}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="PickUpStackNavigator"
          component={PickUpStackNavigator}
        />
        <Stack.Screen
          name="SuccessPopup"
          component={SuccessPopup}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </>
  );
};

export default Base;
