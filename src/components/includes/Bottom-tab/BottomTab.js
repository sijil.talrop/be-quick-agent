import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  Animated,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const {height, width} = Dimensions.get('window');

export default function BottomTab() {
  const navigation = useNavigation();
  const opacityValue = useRef(new Animated.Value(0)).current;
  const bottomValue = useRef(new Animated.Value(50)).current;
  const LeftValue = useRef(new Animated.Value(-5)).current;

  const jumpUp = () => {
    Animated.spring(bottomValue, {
      useNativeDriver: true,
      toValue: 0,
      duration: 400,
    }).start();
  };

  const fadeIn = () => {
    Animated.timing(opacityValue, {
      useNativeDriver: true,
      toValue: 1,
      duration: 300,
    }).start();
  };

  const moveText = () => {
    Animated.timing(LeftValue, {
      useNativeDriver: true,
      toValue: 7,
      duration: 400,
    }).start();
  };

  useEffect(() => {
    jumpUp();
    fadeIn();
    moveText();
  }, []);

  const [isActive, setActive] = useState('home');
  const handleTabActive = (value) => {
    setActive(value);
  };

  const [currentPage, setCurrentPage] = useState();

  const TabIcon = [
    {
      navigation: 'HomeStackNavigator',
      icon: 'home',
    },
    {
      navigation: 'BaseStackNavigator',
      icon: 'search',
    },
    {
      icon: 'business-center',
    },
    {
      icon: 'favorite-border',
    },
    {
      icon: 'widgets',
    },
  ];

  return (
    <Animated.View
      style={[
        styles.container,
        {transform: [{translateY: bottomValue}], opacity: opacityValue},
      ]}>
      {TabIcon.map((item) => (
        <TouchableOpacity
          onPress={() => {
            handleTabActive(item.navigation);
            // navigation.navigate(item.navigation);
          }}
          style={styles.itemContainer}>
          <Icon
            name={item.icon}
            color={'#6d4067'}
            size={30}
            style={{color: item.navigation === isActive ? 'red' : '#ddd'}}
          />
          {/* <Animated.View
            style={[
              {
                transform: [{translateX: LeftValue}],
                opacity: opacityValue,
              },
            ]}></Animated.View> */}
        </TouchableOpacity>
      ))}
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 35,
    backgroundColor: '#aaa',
    height: 60,
    width: width - 15,
    position: 'absolute',
    bottom: 25,
    zIndex: 1000,
    alignSelf: 'center',
    borderRadius: 20,
  },
  itemContainer: {display: 'flex', flexDirection: 'row', alignItems: 'center'},
  iconContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: 40,
  },
  text: {
    color: 'red',
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    transform: [{translateY: 2}],
  },
});
