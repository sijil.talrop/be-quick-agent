import React, {useRef, useEffect, useState, useContext} from 'react';
import {
  Animated,
  ImageBackground,
  StyleSheet,
  View,
  TextInput,
  Dimensions,
  Text,
  TouchableWithoutFeedback,
  Image,
  ActivityIndicator,
  Keyboard,
} from 'react-native';
import {BASE_URL} from '../../../../Settings';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LottieView from 'lottie-react-native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import {UserContext} from '../../../contexts/stores/UserStore';
// import CheckBox from '@react-native-community/checkbox';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import posed from 'react-native-pose';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
import Icon from '../../../assets/icons1/bqdelex.svg';
import {useNavigation} from '@react-navigation/native';

const {width, height} = Dimensions.get('window');
const transition = {type: 'spring', stiffness: 100, damping: 15};
const Wrapper = posed.View({
  big: {scale: 1, rotate: '360deg', transition},
  small: {scale: 0, rotate: '0deg', transition},
});

export default function Login() {
  const navigation = useNavigation();
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [secureText, setSecureText] = useState(true);
  const [focused, setFocused] = useState(false);
  const [loading, setLoading] = useState(false);
  const bottomValue = useRef(new Animated.Value(100)).current;
  const opacityValue = useRef(new Animated.Value(0)).current;
  const opacityText = useRef(new Animated.Value(0)).current;
  const [isBig, setIsBig] = React.useState(false);
  const {userState, userDispatch} = useContext(UserContext);

  const jumpUp = () => {
    Animated.spring(bottomValue, {
      useNativeDriver: true,
      toValue: 0,
      duration: 9000,
    }).start();
  };
  const fadeIn = () => {
    Animated.timing(opacityValue, {
      useNativeDriver: true,
      toValue: 1,
      duration: 900,
    }).start();
  };
  const textFade = () => {
    Animated.timing(opacityText, {
      toValue: 2,
      duration: 3500,
      useNativeDriver: true,
    }).start();
  };

  useEffect(() => {
    jumpUp();
    textFade();
    setIsBig(!isBig);
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        setIsBig(false); // or some other action
      },
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setIsBig(true); // or some other action
      },
    );

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, [500]);

  const addAccount = async user => {
    console.log(user);
    await AsyncStorage.setItem('user', JSON.stringify(user)).then(result => {
      Toast.show('Login successfully', Toast.SHORT);
      // navigation.navigate('Home');
      setLoading(false);
      userDispatch({
        type: 'UPDATE_USER',
        user: {
          user: 'user',
        },
      });
    });
  };

  const Login = () => {
    setLoading(true);
    // setLoading(true);
    let post_url = BASE_URL + 'delivery-agents/login/';
    console.log(post_url);
    axios
      .post(post_url, {
        user_name: username,
        password: password,
      })
      .then(response => {
        if (response.data.StatusCode == 6000) {
          setTimeout(() => {
            let user = {
              access_token: response.data.data.access,
              role: 'user',
            };
            addAccount(user);
          }, 500);
        } else if (response.data.StatusCode == 6001) {
          setTimeout(() => {
            Toast.show(response.data.message, Toast.SHORT);
            setLoading(false);
          }, 500);
        }
      })
      .catch(error => {
        setTimeout(() => {
          console.warn(error, post_url);
          setLoading(false);
        }, 500);
      });
  };

  return (
    <>
      <View style={styles.container}>
        <ImageBackground
          source={require('../../../assets/images/Group20260.png')}
          style={styles.backgroundImage}></ImageBackground>
        <TouchableWithoutFeedback
        //  onPress={() => setIsBig(!isBig)}
        >
          <View
            style={{
              // paddingTop: height * 0.2,
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {/* {!focused && ( */}
            <Wrapper pose={isBig ? 'big' : 'small'}>
              {/* <Image
                  source={require('../../../assets/icons1/2icon.png')}
                  resizeMode="cover"
                  style={{width: width * 0.4, height: width * 0.4}}
                /> */}
              <Icon width="130" height="130" />
            </Wrapper>
            {/* )} */}
          </View>
        </TouchableWithoutFeedback>
        <Animated.View
          style={[
            styles.contentContainer,
            {
              transform: [{translateY: bottomValue}],
              // opacity: opacityValue,
            },
          ]}>
          <View style={styles.top}>
            <Animated.Text style={[styles.loginHead, {opacity: opacityText}]}>
              Login
            </Animated.Text>
            <Text style={styles.description}>
              Welcome to your Agent Account
            </Text>
          </View>
          <View style={styles.bottom}>
            <View style={{marginBottom: 20}}>
              <Text style={styles.inputHead}>Username/Email</Text>
              <TextInput
                width={'100%'}
                style={styles.textInput}
                onChangeText={text => setUsername(text)}
                value={username}
              />
            </View>
            <View>
              <Text style={styles.inputHead}>Password</Text>
              <View>
                <TextInput
                  style={styles.textInput}
                  onChangeText={text => setPassword(text)}
                  value={password}
                  secureTextEntry={secureText}
                />
                <View style={styles.eyeContainer}>
                  <Icon1
                    name={secureText ? 'eye-off-outline' : 'eye'}
                    size={20}
                    color="#c6c6c6"
                    onPress={() => {
                      setSecureText(!secureText);
                    }}
                  />
                </View>
              </View>
            </View>
            <View style={{alignSelf: 'flex-end'}}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('ForgetPassword');
                }}
                activeOpacity={0.7}>
                <Text style={styles.forget}>Forget password?</Text>
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              Login();
            }}
            style={styles.loginButton}>
            {loading ? (
              <LottieView
                source={require('../../../assets/lotties/loading.json')}
                autoPlay
                style={{height: 50, width: 50}}
                loop
              />
            ) : (
              <Text style={styles.loginText}>Login</Text>
            )}
          </TouchableOpacity>
        </Animated.View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    minHeight: '100%',
    backgroundColor: '#f5f5f5',
  },
  backgroundImage: {
    flex: 1,
    // justifyContent: 'flex-end',
    resizeMode: 'cover',
    // justifyContent: 'flex-end',
    width: width * 0.4,
    height: height * 0.3,
    position: 'absolute',
    right: 0,
    zIndex: -1,
  },
  contentContainer: {
    width: width,
    paddingHorizontal: 30,
    paddingVertical: 32,
    justifyContent: 'center',
  },
  checkBoxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: height * 0.02,
  },
  checkBoxText: {
    color: '#737373',
    fontFamily: 'Poppins-SemiBold',
    marginLeft: 15,
  },
  top: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 31,
  },
  textInput: {
    backgroundColor: '#fff',
    borderRadius: 30,
    paddingHorizontal: 25,
    fontFamily: 'Poppins-Regular',
    color: '#737373',
  },
  inputHead: {
    marginBottom: 10,
    fontSize: RFValue(14, height),
    color: '#737373',
    fontFamily: 'Poppins-Regular',
  },
  loginButton: {
    backgroundColor: '#85bc3a',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 35,
    height: 50,
    // paddingVertical: 10,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: RFValue(14, height),
    fontFamily: 'Poppins-SemiBold',
  },
  bottomText: {
    fontSize: RFValue(14, height),
    color: '#fff',
    fontFamily: 'Poppins-SemiBold',
  },
  loginHead: {
    fontSize: RFValue(22, height),
    color: '#000',
    fontFamily: 'Poppins-SemiBold',
  },
  createText: {
    color: '#85bc3a',
    fontSize: RFValue(14, height),
    marginLeft: 10,
    fontFamily: 'Poppins-SemiBold',
  },
  description: {
    textAlign: 'center',
    fontSize: RFValue(15, height),
    marginTop: 5,
    color: '#737373',
    fontFamily: 'Poppins-Regular',
  },
  loginText: {
    textAlign: 'center',
    fontSize: RFValue(15, height),
    color: '#fff',
    fontFamily: 'Poppins-SemiBold',
  },
  eyeContainer: {
    position: 'absolute',
    right: 10,
    top: 15,
  },
  forget: {
    color: '#8ec641',
    fontFamily: 'Poppins-Regular',
    marginTop: 15,
    textAlign: 'right',
    fontSize: RFValue(12, height),
  },
});
