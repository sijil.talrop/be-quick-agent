import React, {useState, useContext, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  ImageBackground,
  Keyboard,
  Animated,
  TextInput,
  Dimensions,
} from 'react-native';
const {width, height} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import IconGreen from '../../../assets/icons1/bqdelex.svg';
import LottieView from 'lottie-react-native';
import Toast from 'react-native-simple-toast';

import {BASE_URL} from '../../../../Settings';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function ForgetPassword() {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [rePassword, setRePassword] = useState('');
  const [focusElement, setFocusElement] = useState('');
  const [secureText1, setSecureText1] = useState(true);
  const [secureText2, setSecureText2] = useState(true);

  const bottomValue = useRef(new Animated.Value(100)).current;
  const opacityValue = useRef(new Animated.Value(0)).current;

  const inputEl1 = useRef();
  const inputEl2 = useRef();

  useEffect(() => {
    inputEl1.current.setNativeProps({
      style: {fontFamily: 'Poppins-Regular'},
    });
    inputEl2.current.setNativeProps({
      style: {fontFamily: 'Poppins-Regular'},
    });
  }, []);

  const jumpUp = () => {
    Animated.spring(bottomValue, {
      useNativeDriver: true,
      toValue: 0,
      duration: 1000,
    }).start();
  };

  const fadeIn = () => {
    Animated.timing(opacityValue, {
      useNativeDriver: true,
      toValue: 1,
      duration: 2000,
    }).start();
  };

  useEffect(() => {
    jumpUp();
    fadeIn();
  }, []);

  const forgetPassword = () => {
    let post_url = BASE_URL + 'delivery-agents/forgot-password/';
    console.log(post_url);
    axios
      .post(post_url, {
        phone: phone,
        password: password,
      })
      .then(res => {
        let {StatusCode, data} = res.data;
        if (StatusCode == 6000) {
          setTimeout(() => {
            navigation.navigate('ForgetVerification', {
              phone: phone,
              password: password,
            });
            console.log(res.data);
            Toast.show(res.data.message, Toast.LONG);
            setLoading(false);
          }, 500);
        } else {
          setTimeout(() => {
            setLoading(false);
            Toast.show(res.data.message, Toast.LONG);
          }, 500);
        }
      })
      .catch(error => {
        setLoading(false);
      });
  };

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <View style={styles.image}>
          <IconGreen height={130} width={130} />
        </View>
      </View>
      <Animated.View
        style={[
          styles.inputContainer,
          {transform: [{translateY: bottomValue}]},
        ]}>
        <Text style={styles.head}>Forget Password</Text>
        <View>
          <TextInput
            value={phone}
            style={[
              styles.input,
              {
                marginBottom: 20,
                borderColor: focusElement == 'phone' ? '#8ec641' : '#fbfbfb',
                backgroundColor: focusElement == 'phone' ? '#fff' : '#fbfbfb',
              },
            ]}
            placeholder="Phone"
            placeholderTextColor={'#C7C7CD'}
            returnKeyType={'next'}
            onSubmitEditing={() => inputEl1.current.focus()}
            onChangeText={val => setPhone(val)}
            keyboardType="numeric"
            onFocus={() => {
              setFocusElement('phone');
            }}
          />
          <View>
            <TextInput
              value={password}
              style={[
                styles.input,

                {
                  marginBottom: 20,
                  borderColor:
                    focusElement == 'password' ? '#8ec641' : '#fbfbfb',
                  backgroundColor:
                    focusElement == 'password' ? '#fff' : '#fbfbfb',
                },
              ]}
              ref={inputEl1}
              placeholder="New Password"
              placeholderTextColor={'#C7C7CD'}
              returnKeyType={'next'}
              onSubmitEditing={() => inputEl2.current.focus()}
              onChangeText={val => setPassword(val)}
              secureTextEntry={secureText1}
              onFocus={() => {
                setFocusElement('password');
              }}
            />
            <View style={styles.eyeContainer}>
              <Icon
                name={secureText1 ? 'eye-off-outline' : 'eye'}
                size={20}
                color="#c6c6c6"
                onPress={() => {
                  setSecureText1(!secureText1);
                }}
              />
            </View>
          </View>

          <View>
            <TextInput
              value={rePassword}
              style={[
                styles.input,
                {
                  borderColor:
                    focusElement == 'rePassword' ? '#8ec641' : '#fbfbfb',
                  backgroundColor:
                    focusElement == 'rePassword' ? '#fff' : '#fbfbfb',
                },
              ]}
              ref={inputEl2}
              placeholder="Confirm Password"
              placeholderTextColor={'#C7C7CD'}
              returnKeyType={'done'}
              onSubmitEditing={() => console.log('submit')}
              onChangeText={val => setRePassword(val)}
              secureTextEntry={secureText2}
              onFocus={() => {
                setFocusElement('rePassword');
              }}
            />
            <View style={styles.eyeContainer}>
              <Icon
                name={secureText2 ? 'eye-off-outline' : 'eye'}
                size={20}
                color="#c6c6c6"
                onPress={() => {
                  setSecureText2(!secureText2);
                }}
              />
            </View>
          </View>
        </View>

        <TouchableOpacity
          onPress={() => {
            setLoading(true);
            setTimeout(() => {
              if (password === rePassword) {
                forgetPassword();
                Keyboard.dismiss();
              } else {
                Toast.show('Password mismatches', Toast.LONG);
                setLoading(false);
              }
            }, 500);
          }}
          style={styles.login}
          activeOpacity={0.7}>
          {loading ? (
            // <ActivityIndicator size={28} color="#fff" />
            <LottieView
              source={require('../../../assets/lotties/loading.json')}
              autoPlay
              style={{height: 50, width: 50}}
              loop
            />
          ) : (
            <Text style={styles.loginText}>Submit</Text>
          )}
        </TouchableOpacity>
      </Animated.View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#fff',
    flexDirection: 'column',
    // alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  logoContainer: {
    height: '40%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputContainer: {
    height: '60%',
    justifyContent: 'space-around',
  },
  head: {
    color: '#484848',
    fontFamily: 'Poppins-SemiBold',
    textAlign: 'center',
    fontSize: RFValue(22, height),
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: RFValue(13, height),
    borderWidth: 1,
    paddingHorizontal: 15,
    backgroundColor: '#fbfbfb',
    borderRadius: 10,
  },
  login: {
    backgroundColor: '#8ec641',
    width: '100%',
    borderRadius: 10,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    // marginTop: 50,
    // paddingVertical: 10,
  },
  loginText: {
    fontFamily: 'Poppins-SemiBold',
    textAlign: 'center',
    color: '#fff',
    // fontSize: 17,
    fontSize: RFValue(15, height),
    // paddingVertical: 13,
  },
  eyeContainer: {
    position: 'absolute',
    right: 10,
    top: 14,
  },
});
