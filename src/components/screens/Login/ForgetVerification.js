import React, {useState, useContext, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  ImageBackground,
  Animated,
  TextInput,
  Dimensions,
} from 'react-native';
const {width, height} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

import OTPInputView from '@twotalltotems/react-native-otp-input';
import IconGreen from '../../../assets/icons1/bqdelex.svg';
import LottieView from 'lottie-react-native';
import Toast from 'react-native-simple-toast';

import {BASE_URL} from '../../../../Settings';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function ForgetVerification({route}) {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const [otp, setOtp] = useState('');

  const handleVerify = () => {
    let post_url = BASE_URL + 'delivery-agents/verify-otp-password/';
    console.log(post_url);
    axios
      .post(post_url, {
        phone: route.params.phone,
        otp: otp,
        password: route.params.password,
      })
      .then(res => {
        let {StatusCode, data} = res.data;
        if (StatusCode == 6000) {
          setTimeout(() => {
            Toast.show(res.data.message, Toast.LONG);
            navigation.navigate('LoginPage');
            setLoading(false);
          }, 500);
        } else {
          setTimeout(() => {
            setLoading(false);
            Toast.show(res.data.message, Toast.SHORT);
          }, 500);
        }
      })
      .catch(error => {
        console.log(error);
        setLoading(false);
      });
  };

  const resendOtp = () => {
    let post_url = BASE_URL + 'delivery-agents/resend-otp/';
    console.log(post_url);
    axios
      .post(post_url, {
        phone: route.params.phone,
      })
      .then(res => {
        let {StatusCode, data} = res.data;
        if (StatusCode == 6000) {
          setTimeout(() => {
            console.warn(res.data);
            Toast.show(
              'We have sent an otp to your mobile number',
              Toast.SHORT,
            );
          }, 500);
        } else {
          setTimeout(() => {
            console.warn(res.data);
          }, 500);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.contents}>
          {/* <Image
            source={require('../../assets/icons/green.png')}
            style={{width: 200, height: 200, resizeMode: 'contain'}}
          /> */}
          <View style={{marginBottom: 30}}>
            <IconGreen height={130} width={130} />
          </View>

          <Text style={styles.head}>Verify OTP</Text>
          <Text style={styles.subText}>Please verify the OTP we are</Text>
          <Text style={styles.subText}>
            sent to your phone number {route.params.phone}
          </Text>
          <TouchableOpacity
            onPress={() => {
              resendOtp();
            }}
            activeOpacity={0.7}>
            <Text style={styles.resend}>Resend OTP</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.input}>
          <OTPInputView
            style={{width: '80%', height: 200}}
            pinCount={4}
            // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
            // onCodeChanged = {code => { this.setState({code})}}
            autoFocusOnLoad
            codeInputFieldStyle={styles.underlineStyleBase}
            codeInputHighlightStyle={styles.underlineStyleHighLighted}
            onCodeFilled={code => {
              setOtp(code);
              console.log(`Code is ${code}, you are good to go!`);
            }}
          />

          <TouchableOpacity
            onPress={() => {
              // navigation.navigate('IntroComponent');
              setLoading(true);
              handleVerify();
            }}
            style={styles.verify}
            activeOpacity={0.7}>
            {loading ? (
              // <ActivityIndicator size={28} color="#fff" />
              <LottieView
                source={require('../../../assets/lotties/loading.json')}
                autoPlay
                style={{height: 50, width: 50}}
                loop
              />
            ) : (
              <Text style={styles.verifyText}>Verify</Text>
            )}
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#fff',
  },
  contents: {
    height: height * 0.6,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  input: {
    height: height * 0.4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  head: {
    color: '#484848',
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(25, height),
    marginBottom: 30,
  },
  subText: {
    fontFamily: 'Poppins-Regular',
    color: '#484848',
    fontSize: RFValue(14, height),
  },

  //   borderStyleBase: {
  //     width: 30,
  //     height: 45,
  //   },

  borderStyleHighLighted: {
    borderColor: '#8ec641',
  },
  codeInputFieldStyle: {
    color: '#000',
  },
  underlineStyleBase: {
    // width: 30,
    // height: 45,
    // borderBottomWidth: 1,
    borderWidth: 1,
    backgroundColor: '#fbfbfb',
    borderRadius: 10,
  },

  underlineStyleHighLighted: {
    borderColor: '#8ec641',
  },
  verify: {
    backgroundColor: '#8ec641',
    width: '80%',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  verifyText: {
    fontFamily: 'Poppins-SemiBold',
    textAlign: 'center',
    color: '#fff',
    fontSize: RFValue(17, height),
    // paddingVertical: 12,
  },
  resend: {
    color: '#8ec641',
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(15, height),
    textAlign: 'center',
    marginTop: 20,
  },
});
