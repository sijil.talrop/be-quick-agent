import React, {useState, useEffect, useRef} from 'react';
const {width, height} = Dimensions.get('window');

import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BasicHeading from '../../screens/heading/BasicHeading';
// import {SafeAreaView} from 'react-native-safe-area-context';
const Reward_Box_Height = width * 0.3;
const card_width = width * 0.9;

const NotificationPage = () => {
  const [like, setLike] = useState(true);
  const UnreadNotification = [
    {
      id: 1,
      image:
        'https://s3.ap-south-1.amazonaws.com/tcsonline-live/catalog/category/Boys-dress1_1.jpg',
      product_name: 'shop',
      price: '30',
      product_rate: 'unwatch',
      deliver_rate: 'unwatch',
    },
    {
      id: 2,
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSM227g9Bjn88BMbi1ov8eSDPv0vJpjCSrgrg&usqp=CAU',
      product_name: 'Games',
      price: '45',
      product_rate: 'unwatch',
      deliver_rate: 'watch',
    },
    {
      id: 3,
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRqgr-9yIkuJ2Fh5sgbk94UqYxxKaMt7yTp1w&usqp=CAU',
      product_name: 'Quiz',
      price: '20',
      product_rate: 'unwatch',
      deliver_rate: 'unwatch',
    },
  ];
  const ReadNotification = [
    {
      id: 1,
      product_name: 'shop',
      price: '30',
      deliver_rate: 'unwatch',
    },
    {
      id: 2,
      product_name: 'Games',
      price: '45',
      deliver_rate: 'watch',
    },
    {
      id: 3,
      product_name: 'Quiz',
      price: '20',
      deliver_rate: 'watch',
    },
  ];

  const Unread = () => {
    return UnreadNotification.map((item, index) => (
      <TouchableOpacity
        key={index}
        activeOpacity={0.9}
        style={styles.mainContainer}>
        <View style={styles.cartCard}>
          <View style={styles.imageBox}>
            <Text style={styles.mainDetail}>TS</Text>
          </View>
          <View style={[styles.MonthStatus, {marginLeft: 10}]}>
            <Text style={styles.Content}>You have a new service request</Text>
            <Text style={styles.DateFont}>8:30pm ,12th Feb</Text>
          </View>
        </View>
      </TouchableOpacity>
    ));
  };

  const Read = () => {
    return ReadNotification.map((item, index) => (
      <TouchableOpacity
        key={index}
        activeOpacity={0.9}
        style={styles.mainContainer}>
        <View style={styles.cartCard}>
          <View style={[styles.imageBox, {backgroundColor: '#9c9c9c'}]}>
            <Text style={styles.mainDetail}>TS</Text>
          </View>
          <View style={[styles.MonthStatus, {marginLeft: 10}]}>
            <Text style={styles.Content}>You have a new service request</Text>
            <Text style={styles.DateFont}>8:30pm ,12th Feb</Text>
          </View>
        </View>
      </TouchableOpacity>
    ));
  };

  return (
    <>
      <View style={{backgroundColor: '#fff'}}>
        <View style={styles.headContainer}>
          <BasicHeading title="Notification" />
        </View>
        <ScrollView
          contentContainerStyle={{backgroundColor: '#fff', minHeight: '100%'}}>
          <View style={styles.contentBox}>
            <Text style={styles.Head}>UnRead</Text>
            {Unread()}
          </View>
          <View style={styles.contentBox}>
            <Text style={styles.Head}>read</Text>
            {Read()}
          </View>
        </ScrollView>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    backgroundColor: '#fff',
    marginVertical: 15,
    borderRadius: 10,
  },
  contentBox: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  cartCard: {
    width: card_width,
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 0.7,
    borderColor: '#aaa',
    paddingVertical: 10,
  },
  imageBox: {
    width: 60,
    height: 60,
    overflow: 'hidden',
    borderRadius: 30,
    backgroundColor: '#f57952',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Head: {
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
    fontSize: 18,
  },
  headContainer: {
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#fff',
  },
  MonthStatus: {},
  DateFont: {
    color: '#000',
    fontSize: 13,
    fontFamily: 'Poppins-LightItalic',
  },
  Content: {
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
    fontSize: 14,
  },
  mainDetail: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 23,
    color: '#fff',
  },
});

export default NotificationPage;
