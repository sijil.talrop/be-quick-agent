import React, {useRef, useEffect, useState, useContext} from 'react';
import {
  StyleSheet,
  View,
  Animated,
  Dimensions,
  Text,
  Image,
  TouchableOpacity,
  Switch,
  ScrollView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
const {width, height} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {UserContext} from '../../../contexts/stores/UserStore';
import {BASE_URL} from '../../../../Settings';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Close from '../../../assets/icons2/close-menu.svg';
import Delivery from '../../../assets/icons2/delivery.svg';
import Dashboard from '../../../assets/icons2/dashboard.svg';
import DashboardEnable from '../../../assets/icons2/dashboard-enable.svg';
import Notification from '../../../assets/icons2/notifications.svg';
import NotificationEnable from '../../../assets/icons2/notifications-enable.svg';
import Orders from '../../../assets/icons2/orders.svg';
import OrdersEnable from '../../../assets/icons2/orders-enable.svg';
import Profile from '../../../assets/icons2/profile.svg';
import ProfileEnable from '../../../assets/icons2/profile-enable.svg';
import Help from '../../../assets/icons2/help.svg';
import HelpEnable from '../../../assets/icons2/help-enable.svg';

export default function Menu(props) {
  const {userState, userDispatch} = useContext(UserContext);
  const navigation = useNavigation();
  const [isOpen, setIsOpen] = useState(false);
  const [selected, setSelected] = useState('');
  const [switchValue, setSwitchValue] = useState(userState.is_duty);

  const toggleSwitch = value => {
    if (props.type == 'duty_option') {
      props.setSwitchValue(!switchValue);
      props.setMenuModal(false);
    } else {
      setDuty(value);
      props.setMenuModal(false);
    }
  };

  const toggleSwitchValue = value => {
    setSelected(value);
  };

  useEffect(() => {
    console.log(selected, '////////');
  }, [selected]);

  const setDuty = async value => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'delivery-agents/duty-status/';
    console.warn(post_url, switchValue);
    axios
      .post(
        post_url,
        {
          data: value,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode} = response.data;
        if (StatusCode == 6000) {
          userDispatch({
            type: 'UPDATE_USER',
            user: {
              is_duty: value,
            },
          });
          navigation.navigate('DutyOptionPage');
        } else {
          console.warn(response.data);
        }
      })
      .catch(error => {
        console.log(error.response);
      });
  };

  return (
    <Animated.View style={[styles.mainContentBox]}>
      <ScrollView>
        <View
          style={[
            styles.mainSwitchContainer,
            {backgroundColor: switchValue ? '#f0784f' : '#9d9d9d'},
          ]}>
          <View style={styles.imageBox}>
            {/* <Image
                style={styles.image}
                source={require('../../../assets/icons2/Group6058.png')}
              /> */}
            <Delivery width={30} height={30} />
          </View>
          <View style={[styles.container2]}>
            <Text style={styles.topText}>
              Duty{' '}
              {switchValue ? (
                <Text style={styles.lightText}>
                  ( Live for {userState.live_time} )
                </Text>
              ) : (
                <Text style={styles.lightText}>( off )</Text>
              )}
            </Text>
            <Switch
              onValueChange={toggleSwitch}
              value={switchValue}
              trackColor={{false: '#fff', true: '#fff'}}
              thumbColor={switchValue ? '#f57952' : '#9d9d9d'}
              style={{transform: [{scaleX: 1.6}, {scaleY: 1.6}]}}
            />
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            toggleSwitchValue('Dashboard');
            setTimeout(() => {
              navigation.navigate('DashBoardPage');
            }, 500);
            setTimeout(() => {
              props.setMenuModal(false);
            }, 550);
          }}
          activeOpacity={0.7}
          style={[
            styles.menuContent,
            {
              borderColor: 'Dashboard' == selected ? '#f0784f' : '#fff',
              backgroundColor: 'Dashboard' == selected ? '#f0784f' : '#f6f6f6',
            },
          ]}>
          <View style={styles.imageBox}>
            {'Dashboard' === selected ? (
              // <Image
              //   style={styles.image}
              //   source={require('../../../assets/icons2/dashboard-1.png')}
              // />
              <DashboardEnable width={30} height={30} />
            ) : (
              // <Image
              //   style={styles.image}
              //   source={require('../../../assets/icons2/dashboard-1.png')}
              // />
              <Dashboard width={30} height={30} />
            )}
          </View>
          <Text
            style={[
              styles.menuText,
              {color: 'Dashboard' == selected ? '#fff' : '#070707'},
            ]}>
            Dashboard
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            toggleSwitchValue('Notification');
            setTimeout(() => {
              navigation.navigate('NotificationPage');
            }, 500);
            setTimeout(() => {
              props.setMenuModal(false);
            }, 550);
          }}
          activeOpacity={0.7}
          style={[
            styles.menuContent,
            {
              borderColor: 'Notification' == selected ? '#f0784f' : '#fff',
              backgroundColor:
                'Notification' == selected ? '#f0784f' : '#f6f6f6',
            },
          ]}>
          <View style={styles.imageBox}>
            {'Notification' === selected ? (
              <NotificationEnable width={30} height={30} />
            ) : (
              <Notification width={30} height={30} />
            )}
          </View>
          <Text
            style={[
              styles.menuText,
              {color: 'Notification' == selected ? '#fff' : '#070707'},
            ]}>
            Notifications
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            toggleSwitchValue('Orders');
            setTimeout(() => {
              navigation.navigate('OrdersTab');
            }, 500);
            setTimeout(() => {
              props.setMenuModal(false);
            }, 550);
          }}
          activeOpacity={0.7}
          style={[
            styles.menuContent,
            {
              borderColor: 'Orders' == selected ? '#f0784f' : '#fff',
              backgroundColor: 'Orders' == selected ? '#f0784f' : '#f6f6f6',
            },
          ]}>
          <View style={styles.imageBox}>
            {'Orders' === selected ? (
              <OrdersEnable width={30} height={30} />
            ) : (
              <Orders width={30} height={30} />
            )}
          </View>
          <Text
            style={[
              styles.menuText,
              {color: 'Orders' == selected ? '#fff' : '#070707'},
            ]}>
            Orders
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            toggleSwitchValue('Profile');
            setTimeout(() => {
              navigation.navigate('Profile');
            }, 500);
            setTimeout(() => {
              props.setMenuModal(false);
            }, 550);
          }}
          activeOpacity={0.7}
          style={[
            styles.menuContent,
            {
              borderColor: 'Profile' == selected ? '#f0784f' : '#fff',
              backgroundColor: 'Profile' == selected ? '#f0784f' : '#f6f6f6',
            },
          ]}>
          <View style={styles.imageBox}>
            {'Profile' === selected ? (
              <ProfileEnable width={30} height={30} />
            ) : (
              <Profile width={30} height={30} />
            )}
          </View>
          <Text
            style={[
              styles.menuText,
              {color: 'Profile' == selected ? '#fff' : '#070707'},
            ]}>
            Profile
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            toggleSwitchValue('Help');
            setTimeout(() => {
              navigation.navigate('Help');
            }, 500);
            setTimeout(() => {
              props.setMenuModal(false);
            }, 550);
          }}
          activeOpacity={0.7}
          style={[
            styles.menuContent,
            {
              borderColor: 'Help' == selected ? '#f0784f' : '#fff',
              backgroundColor: 'Help' == selected ? '#f0784f' : '#f6f6f6',
            },
          ]}>
          <View style={styles.imageBox}>
            {'Help' === selected ? (
              <HelpEnable width={30} height={30} />
            ) : (
              <Help width={30} height={30} />
            )}
          </View>
          <Text
            style={[
              styles.menuText,
              {color: 'Help' == selected ? '#fff' : '#070707'},
            ]}>
            Help
          </Text>
        </TouchableOpacity>

        <View style={{alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() => props.setMenuModal(false)}
            activeOpacity={0.7}
            style={styles.bottomIcon}>
            {/* <Image
                style={styles.image}
                source={require('../../../assets/icons1/close_menu.png')}
              /> */}
            <Close width={30} height={30} />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  mainSwitchContainer: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    justifyContent: 'space-between',
    paddingVertical: 20,
    borderRadius: 15,
    marginBottom: 20,
  },
  mainContentBox: {
    backgroundColor: '#fff',
    paddingHorizontal: 20,
    paddingVertical: 50,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 1,
    borderColor: '#eee',
  },
  imageBox: {
    width: 30,
    height: 30,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
  },
  bottomIcon: {
    // width: 70,
    // height: 70,
    marginVertical: 15,
  },
  topText: {
    fontFamily: 'Poppins-SemiBold',
    color: '#fff',
    fontSize: RFValue(13, height),
  },
  lightText: {
    fontFamily: 'Poppins-Regular',
  },
  container2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: width * 0.7,
  },
  menuContent: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginVertical: 10,
    borderRadius: 15,
    borderWidth: 1,
  },
  menuText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(12, height),
    marginLeft: 20,
    paddingVertical: 18,
  },
});
