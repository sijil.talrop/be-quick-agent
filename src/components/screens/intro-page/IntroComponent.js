import React, {useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';
import posed from 'react-native-pose';
var {height, width} = Dimensions.get('window');
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import VersionNumber from 'react-native-version-number';
const transition = {type: 'tween', stiffness: 100, damping: 15};
import {LinearTextGradient} from 'react-native-text-gradient';
const Wrapper = posed.View({
  big: {scale: 1, rotate: '360deg', transition},
  small: {scale: 0.1, rotate: '0deg', transition},
});
import Icon from '../../../assets/icons1/bqdelex.svg';

const IntroComponent = ({route}) => {
  const [isBig, setIsBig] = React.useState(false);

  useEffect(() => {
    setIsBig(!isBig);
  }, []);

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../../../assets/images/Group20260.png')}
        style={styles.backgroundImage}></ImageBackground>
      <View>
        <TouchableWithoutFeedback
          // onPress={() => setIsBig(!isBig)}
          style={{height: height * 0.6}}>
          <View style={styles.imageContainer}>
            {/* <Wrapper pose={isBig ? 'big' : 'small'}> */}
            {/* <Image
                source={require('../../../assets/icons1/2icon.png')}
                style={styles.image}
              /> */}
            <Icon width="130" height="130" />
            {/* </Wrapper> */}
            <Text style={styles.name}>BQ DelEx</Text>
          </View>
        </TouchableWithoutFeedback>

        <View style={styles.ForgotBox}>
          <Text style={styles.processText}>Powered by</Text>
          {/* <Text style={[styles.subText, {color: '#000', marginBottom: 10}]}>
            BeQuick
          </Text> */}
          <LinearTextGradient
            style={{
              fontFamily: 'Poppins-SemiBold',
              marginBottom: 10,
              fontSize: RFValue(15, height),
            }}
            locations={[0, 1]}
            colors={['#85bc3a', '#f0784f']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}>
            <Text>BeQuick</Text>
          </LinearTextGradient>
          <Text style={styles.subText}>Version {VersionNumber.appVersion}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    // backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-end',
    minHeight: '100%',
    width: width,
    paddingBottom: 30,
  },
  backgroundImage: {
    flex: 1,
    // justifyContent: 'flex-end',
    resizeMode: 'cover',
    // justifyContent: 'flex-end',
    width: width * 0.4,
    height: height * 0.3,
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 5,
  },
  imageContainer: {
    width: width * 0.6,
    height: width * 0.6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  processText: {
    color: '#737373',
    fontFamily: 'Poppins-SemiBold',
    textAlign: 'center',
    width: width * 0.6,
    fontSize: RFValue(15, height),
  },
  subText: {
    fontFamily: 'Poppins-Regular',
    color: '#737373',
    fontSize: RFValue(12, height),
  },
  ForgotBox: {
    alignItems: 'center',
    marginTop: height * 0.3,
  },
  image: {width: width * 0.4, height: width * 0.4},
  name: {
    fontFamily: 'Poppins-Bold',
    color: '#f0784f',
    textAlign: 'center',
    marginTop: 20,
    fontSize: RFValue(15, height),
  },
});

export default IntroComponent;
