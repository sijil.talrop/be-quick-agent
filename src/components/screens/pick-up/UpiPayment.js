import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import {BASE_URL} from '../../../../Settings';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import Modal from 'react-native-modal';
const {width, height} = Dimensions.get('window');
import DeliveryLocated from './DeliveryLocated';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function UpiPayment(props) {
  const [located_modal, setLocated_modal] = useState(false);
  const [mod, setMod] = useState(props.setModalVisible);
  let navigation = useNavigation();

  const deliveryStatus = async data => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'delivery-agents/delivery-status/';
    console.warn(post_url);
    axios
      .post(
        post_url,
        {
          delivery_status: data,
          order_pk: props.orderDetails.id,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          props.setUpiModal(false);
          navigation.navigate('SuccessPopup');
        } else {
          console.log(response.data);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <View style={styles.backgroundImage}>
      <View style={[styles.contentContainer]}>
        <View style={{alignItems: 'center'}}>
          <Text style={styles.headerText}>Delivery Located</Text>
        </View>
        <View style={styles.handoverBox}>
          <View>
            <Text style={styles.leftContent}>
              {props.orderDetails.customer_details.name}
            </Text>
            <Text style={[styles.leftContent, {color: '#000'}]}>
              {props.orderDetails.customer_details.phone}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(
                `tel:${props.orderDetails.customer_details.phone}}`,
              );
            }}
            activeOpacity={0.8}
            style={styles.rightBox}>
            <Icon name={'phone'} color="#fff" size={20} />
            <Text style={styles.rightContent}>Call</Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.loginButton, {backgroundColor: '#f7f7f7'}]}>
          <Text style={[styles.loginText, {color: '#646464'}]}>
            Amount paid through UPI
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            deliveryStatus('delivered');
            // props.setUpiModal(false);
            // navigation.navigate('SuccessPopup');
          }}
          style={styles.loginButton}>
          <Text style={styles.loginText}>Done</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  contentContainer: {
    width: width,
    paddingHorizontal: 30,
    paddingVertical: 32,
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 1,
    borderColor: '#eee',
  },
  loginButton: {
    backgroundColor: '#85bc3a',
    borderRadius: 10,
    alignItems: 'center',
    marginTop: 20,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  headerText: {
    fontSize: RFValue(15, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
  },
  handoverBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginVertical: 10,
    alignItems: 'center',
    borderStyle: 'dashed',
    borderColor: '#aaa',
    borderWidth: 1,
    borderRadius: 10,
    paddingVertical: 16,
  },
  leftContent: {
    fontFamily: 'Poppins-SemiBold',
    color: '#9c9c9c',
    fontSize: RFValue(14, height),
  },
  rightContent: {
    fontSize: RFValue(13, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#fff',
  },
  rightBox: {
    width: width * 0.2,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderRadius: 10,
    backgroundColor: '#f57952',
  },

  loginText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
    color: '#fff',
    textAlign: 'center',
  },
});
