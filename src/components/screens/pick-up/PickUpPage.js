import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ActivityIndicator,
  Linking,
} from 'react-native';
var {height, width} = Dimensions.get('window');
import {BASE_URL} from '../../../../Settings';
import PickUpItem from './PickUpItem';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import AmountHandOver from './AmountHandOver';
import UpiPayment from './UpiPayment';
import DeliveryLocated from './DeliveryLocated';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Modal from 'react-native-modal';
const deviceWidth = Dimensions.get('window').width;

const PickUpPage = ({route}) => {
  const [switchValue, setSwitchValue] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [located_modal, setLocated_modal] = useState(false);
  const [upiModal, setUpiModal] = useState(false);
  const [orderDetails, setOrderDetails] = useState({});
  const [orderItems, setOrderItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const [reload, setReload] = useState(false);
  const [availableForDeliver, setAvailableForDeliver] = useState();
  const [paymentMethod, setPaymentMethod] = useState();

  const [isOpen, setIsOpen] = useState(true);
  const [reach, setReach] = useState();
  const toggleSwitch = value => {
    setSwitchValue(value);
  };

  useEffect(() => {
    console.log(paymentMethod);
    getOrderDetails();
  }, [reload]);

  // useEffect(() => {

  //   pickUpRender();
  // }, [orderDetails]);

  // const checkAvailableForDeliver = data => {
  //   data.forEach(item => {
  //     if (item.order_picked === false) {
  //       console.log(faaaaaaaaaaaaalse);
  //       setAvailableForDeliver(false);
  //       return false;
  //     }
  //   });
  // };

  const getOrderDetails = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let get_url =
      BASE_URL + 'orders/order-details/?order_pk=' + route.params.order_pk;
    console.warn(get_url);
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then(response => {
        let {StatusCode, data} = response.data;
        console.log(data, access_token, '////');
        if (StatusCode === 6000) {
          setTimeout(() => {
            console.log(data.data, '////');
            setOrderDetails(data.data);
            setOrderItems(data.data.pickup_details);
            setReach(data.data.reached_dellivery_location);
            setAvailableForDeliver(data.data.status);
            setPaymentMethod(data.data.payment_method);
            pickUpRender();
            setLoading(false);
          }, 500);
          // checkAvailableForDeliver(data.data.pickup_details);
          // setOrders(data);
        } else {
          setTimeout(() => {
            console.log('no data');
            setLoading(false);
          }, 500);
        }
      })
      .catch(error => {
        setTimeout(() => {
          setLoading(false);
        }, 500);
      });
  };

  const deliveryStatus = async data => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'delivery-agents/delivery-status/';
    console.warn(post_url);
    axios
      .post(
        post_url,
        {
          delivery_status: data,
          order_pk: orderDetails.id,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          setReload(!reload);
        } else {
          console.log(response.data);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const openMap = location => {
    const url = `https://www.google.com/maps/dir/?api=1&destination=${location.latitude},${location.longitude}`;
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  };

  const pickUpRender = () => {
    return (
      <View style={{marginVertical: 20}}>
        <View style={styles.topContainer}>
          <View style={styles.imageBox}>
            <Image
              style={styles.image}
              source={require('../../../assets/icons1/pickup.png')}
            />
          </View>
          <Text style={styles.head}>pickup</Text>
        </View>
        {orderItems.map((item, index) => (
          <View
            key={index}
            style={{
              alignItems: 'center',
              backgroundColor: '#fff',
              paddingTop: 30,
              borderBottomRightRadius: 10,
              borderBottomLeftRadius: 10,
              marginBottom: 5,
            }}>
            <View style={styles.locationBase}>
              <View style={styles.positionContent}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontFamily: 'Poppins-SemiBold',
                  }}>
                  Pickup {index + 1}
                </Text>
              </View>
              <View style={[styles.locationBox]}>
                <View style={styles.locationImageBox}>
                  <Image
                    style={styles.image}
                    source={require('../../../assets/icons1/location.png')}
                  />
                </View>
                <View style={{marginLeft: 20}}>
                  <Text style={styles.shopName}>{item.shop}</Text>
                  <Text style={styles.kmText}>{item.location}</Text>
                </View>
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => {
                  openMap(item.location_details);
                }}>
                <View style={styles.locationImageBox}>
                  <Image
                    style={styles.image}
                    source={require('../../../assets/icons1/direction.png')}
                  />
                </View>
                <Text style={styles.kmText}>{item.distance}</Text>
              </TouchableOpacity>
            </View>
            <PickUpItem
              items={item}
              orderDetails={orderDetails}
              order_pk={route.params.order_pk}
              setReload={setReload}
              reload={reload}
            />
          </View>
        ))}
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../../../assets/images/Group20260.png')}
        style={styles.backgroundImage}></ImageBackground>
      {!loading ? (
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
          style={{minHeight: '100%', paddingTop: height * 0.07}}>
          <View>
            <TouchableOpacity
              onPress={() => {
                // setIsOpen(!isOpen);
              }}
              style={styles.thirdBox}>
              <Text style={styles.idLetter}>
                Order ID:
                <Text style={styles.idMiddleText}>
                  ORD{' '}
                  <Text style={styles.idNumber}>
                    {orderDetails.order_id.slice(
                      orderDetails.order_id.length - 4,
                    )}
                  </Text>
                </Text>
              </Text>
              <Text style={styles.kmText}>{orderDetails.date}</Text>
            </TouchableOpacity>
            {isOpen && <View>{pickUpRender()}</View>}
          </View>
          <View style={{marginVertical: 20}}>
            <View style={styles.topContainer}>
              <View style={styles.imageBox}>
                <Image
                  style={styles.image}
                  source={require('../../../assets/icons1/pickup.png')}
                />
              </View>
              <Text style={styles.head}>Delivery</Text>
            </View>
            <View style={styles.ButtonContainer}>
              <View style={styles.locationBase2}>
                <View style={[styles.locationBox]}>
                  <View style={styles.locationImageBox}>
                    <Image
                      style={styles.image}
                      source={require('../../../assets/icons1/location.png')}
                    />
                  </View>
                  <View style={{marginLeft: 20}}>
                    <Text style={styles.shopName}>
                      {orderDetails.customer_details.name}
                    </Text>
                    <Text style={styles.kmText}>
                      {orderDetails.customer_details.location}
                    </Text>
                  </View>
                </View>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    openMap(orderDetails.customer_details.location_details);
                  }}>
                  <View style={styles.locationImageBox}>
                    <Image
                      style={styles.image}
                      source={require('../../../assets/icons1/direction.png')}
                    />
                  </View>
                  <Text style={styles.kmText}>
                    {orderDetails.customer_details.distance}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.optionalAmount}>
                <Text style={styles.amountPay}>Total amount top pay</Text>
                <Text style={styles.price}>{orderDetails.amount_payable}</Text>
              </View>
              {availableForDeliver === 'pending' ? (
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={[
                    styles.buttonBox,
                    {backgroundColor: '#e6e6e6', flexDirection: 'row'},
                    {
                      backgroundColor: '#e6e6e6',
                      borderColor: '#e6e6e6',
                      borderWidth: 1,
                    },
                  ]}>
                  <Text style={[styles.buttonText, {color: '#fff'}]}>
                    Reached Delivery Location
                  </Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    // setReach(true);
                    deliveryStatus('reached_delivery_location');
                  }}
                  style={[
                    styles.buttonBox,
                    {
                      backgroundColor: '#85bc3a',
                      flexDirection: 'row',
                      justifyContent: reach ? 'space-between' : 'center',
                    },
                    {
                      backgroundColor: reach ? '#fff' : '#85bc3a',
                      borderColor: '#85bc3a',
                      borderWidth: 1,
                    },
                  ]}>
                  <Text
                    style={[
                      styles.buttonText,
                      {color: reach ? '#85bc3a' : '#fff'},
                    ]}>
                    Reached Delivery Location
                  </Text>
                  {reach && (
                    <View style={styles.iconBox}>
                      <Icon name={'check'} color="#fff" size={20} />
                    </View>
                  )}
                </TouchableOpacity>
              )}
              {reach ? (
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    if (paymentMethod === 'cash on delivery') {
                      setModalVisible(true);
                    } else {
                      setUpiModal(true);
                    }
                  }}
                  style={[styles.buttonBox, {backgroundColor: '#85bc3a'}]}>
                  <Text style={styles.buttonText}>Delivered</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={[styles.buttonBox, {backgroundColor: '#e6e6e6'}]}>
                  <Text style={styles.buttonText}>Delivered</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
          <View style={{height: height * 0.3}} />
          <Modal
            onBackButtonPress={() => {
              setModalVisible(false);
            }}
            onBackdropPress={() => {
              setModalVisible(false);
            }}
            isVisible={modalVisible}
            backdropOpacity={0.7}
            style={{
              margin: 0,
              screenBackgroundColor: 'transparent',
              modalPresentationStyle: 'overCurrentContext',
              justifyContent: 'flex-end',
            }}
            propagateSwipe={true}
            useNativeDriver={false}
            onSwipeComplete={() => setModalVisible(false)}
            swipeDirection={['down']}
            animationOut={'fadeOutDown'}
            animationInTiming={500}
            animationOutTiming={500}>
            <AmountHandOver
              setLocated_modal={setLocated_modal}
              setModalVisible={setModalVisible}
              orderDetails={orderDetails}
            />
          </Modal>
          <Modal
            onBackButtonPress={() => {
              setLocated_modal(false);
            }}
            onBackdropPress={() => {
              setLocated_modal(false);
            }}
            isVisible={located_modal}
            backdropOpacity={0.7}
            style={{
              margin: 0,
              screenBackgroundColor: 'transparent',
              modalPresentationStyle: 'overCurrentContext',
              justifyContent: 'flex-end',
            }}
            propagateSwipe={true}
            useNativeDriver={false}
            onSwipeComplete={() => setLocated_modal(false)}
            swipeDirection={['down']}
            animationOut={'fadeOutDown'}
            animationInTiming={500}
            animationOutTiming={500}>
            <DeliveryLocated
              setLocated_modal={setLocated_modal}
              orderDetails={orderDetails}
            />
          </Modal>
          <Modal
            onBackButtonPress={() => {
              setUpiModal(false);
            }}
            onBackdropPress={() => {
              setUpiModal(false);
            }}
            isVisible={upiModal}
            backdropOpacity={0.7}
            style={{
              margin: 0,
              screenBackgroundColor: 'transparent',
              modalPresentationStyle: 'overCurrentContext',
              justifyContent: 'flex-end',
            }}
            propagateSwipe={true}
            useNativeDriver={false}
            onSwipeComplete={() => setUpiModal(false)}
            swipeDirection={['down']}
            animationOut={'fadeOutDown'}
            animationInTiming={500}
            animationOutTiming={500}>
            <UpiPayment setUpiModal={setUpiModal} orderDetails={orderDetails} />
          </Modal>
        </ScrollView>
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size={30} color="#85bc3a" />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  idLetter: {
    fontFamily: 'Poppins-Regular',
    fontSize: RFValue(13, height),
  },
  idMiddleText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  idNumber: {
    backgroundColor: '#b3f9aa',
    fontSize: RFValue(13, height),
  },
  shopName: {
    fontSize: RFValue(13, height),
    fontFamily: 'Poppins-SemiBold',
  },
  kmText: {
    color: '#aaa',
    fontFamily: 'Poppins-Regular',
    // fontSize: 15,
    fontSize: RFValue(12, height),
  },
  head: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
    color: '#fff',
  },
  amountPay: {
    fontFamily: 'Poppins-Regular',
    fontSize: RFValue(13, height),
    color: '#aaa',
  },
  price: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(15, height),
  },
  container: {
    minHeight: '100%',
    width: width,
    paddingHorizontal: 20,
    // paddingTop: height * 0.07,
  },
  thirdBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 20,
  },
  topContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f57952',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    // marginVertical: 15,
    paddingVertical: 10,
  },
  imageBox: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  locationBase: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderStyle: 'dashed',
    borderColor: '#aaa',
    marginTop: 15,
    padding: 10,
    paddingTop: 30,
    width: width * 0.8,
  },
  locationBase2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderStyle: 'dashed',
    borderColor: '#aaa',
    padding: 10,
    width: width * 0.8,
  },
  locationImageBox: {
    width: 25,
    height: 25,
  },
  locationBox: {
    flexDirection: 'row',
    alignItems: 'center',
    width: width * 0.52,
  },
  positionContent: {
    backgroundColor: '#fff',
    position: 'absolute',
    left: width * 0.27,
    right: 0,
    top: -20,
    width: 100,
    borderWidth: 1,
    borderRadius: 30,
    paddingVertical: 10,
    borderStyle: 'dashed',
    borderColor: '#aaa',
    //   bottom: 0,
  },
  buttonBox: {
    width: width * 0.8,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    marginBottom: 15,
  },
  ButtonContainer: {
    width: width * 0.9,
    alignItems: 'center',
    paddingTop: 30,
    backgroundColor: '#fff',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  buttonText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
    color: '#fff',
  },
  optionalAmount: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: width * 0.8,
    marginVertical: 20,
  },
  iconBox: {
    width: 25,
    height: 25,
    backgroundColor: '#85bc3a',
    borderRadius: 30,
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backgroundImage: {
    flex: 1,
    // justifyContent: 'flex-end',
    resizeMode: 'cover',
    // justifyContent: 'flex-end',
    width: width * 0.4,
    height: height * 0.3,
    position: 'absolute',
    right: 0,
    zIndex: -1,
  },
});

export default PickUpPage;
