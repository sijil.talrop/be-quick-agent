import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  UIManager,
  LayoutAnimation,
  Platform,
} from 'react-native';
import {BASE_URL} from '../../../../Settings';
import Modal from 'react-native-modal';
var {height, width} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import {useNavigation} from '@react-navigation/native';
import PickUpLocated from './PickUpLocated';
import RatePickup from './RatePickUp';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const PickUpItem = props => {
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const [rateModal, setRateModal] = useState(false);
  const [switchValue, setSwitchValue] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isPickup, setIsPickup] = useState(props.items.order_picked);
  const [isRated, setIsRated] = useState(props.items.pickup_rating);
  const [icon_Show, setIcon_Show] = useState(
    props.items.reached_pickup_location,
  );
  const toggleSwitch = value => {
    setSwitchValue(value);
  };

  if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  const pickupDetails = async data => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'delivery-agents/pickup-status/';
    console.warn(post_url, data, props.items.id);
    axios
      .post(
        post_url,
        {
          pickup_status: data,
          pickup_pk: props.items.id,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          props.setReload(!props.reload);
          setIcon_Show(true);
        } else {
          console.log(response.data);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <View style={{alignItems: 'center', marginTop: 20}}>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => {
          setIsOpen(!isOpen);
          LayoutAnimation.configureNext({
            duration: 500,
            update: {
              type: LayoutAnimation.Types.spring,
              springDamping: 0.7,
            },
          });
        }}
        style={styles.dropBox}>
        <Text style={styles.mainHead}>Order Details</Text>
        <Icon
          name={isOpen ? 'chevron-up' : 'chevron-down'}
          color="#000"
          size={30}
        />
      </TouchableOpacity>
      {props.items.order_items.map((item, index) => (
        <View
          key={index}
          style={{
            paddingVertical: isOpen ? 10 : 0,
            backgroundColor: '#f5f5f5',
            width: width * 0.8,
          }}>
          {isOpen && (
            <React.Fragment>
              <View style={styles.flexBox}>
                <View style={{width: '60%'}}>
                  <Text style={styles.Items}>
                    {item.product_name} * {Math.trunc(item.qty)}
                  </Text>
                  {/* <Text style={styles.Items}>({item.weight}g)</Text> */}
                </View>
                <Text style={styles.Items}> ₹{item.subtotal}</Text>
              </View>
            </React.Fragment>
          )}
        </View>
      ))}
      <View
        style={[
          styles.flexBox,
          {
            borderTopWidth: 0.5,
            borderTopColor: '#aaa',
            backgroundColor: '#f5f5f5',
            paddingHorizontal: 10,
            paddingVertical: 10,
            borderBottomLeftRadius: 10,
            borderBottomRightRadius: 10,
          },
        ]}>
        <Text style={[styles.total, {fontSize: RFValue(13, height)}]}>
          Grand Total
        </Text>
        <Text style={styles.total}>{props.items.subtotal}</Text>
      </View>
      {!isPickup ? (
        <View style={styles.ButtonContainer}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              pickupDetails('reached_pickup_location');
              // setIcon_Show(true);
            }}
            style={[
              styles.buttonBox,
              {
                backgroundColor: '#85bc3a',
                flexDirection: 'row',
                justifyContent: icon_Show ? 'space-between' : 'center',
                borderColor: '#85bc3a',
                borderWidth: 1,
              },
              {backgroundColor: icon_Show ? '#fff' : '#85bc3a'},
            ]}>
            <Text
              style={[
                styles.buttonText,
                {color: icon_Show ? '#85bc3a' : '#fff'},
              ]}>
              Reached PickUp Location
            </Text>
            {icon_Show && (
              <View style={styles.iconBox}>
                <Icon name={'check'} color="#fff" size={20} />
              </View>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              {
                icon_Show ? setModalVisible(true) : setModalVisible(false);
              }
            }}
            style={[
              styles.buttonBox,
              {backgroundColor: icon_Show ? '#85bc3a' : '#e6e6e6'},
            ]}>
            <Text style={styles.buttonText}>Picked Up</Text>
          </TouchableOpacity>
        </View>
      ) : !isRated ? (
        <View style={styles.ButtonContainer}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.buttonBox,
              {
                backgroundColor: '#fff',
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderColor: '#85bc3a',
                borderWidth: 1,
              },
            ]}>
            <Text style={[styles.buttonText, {color: '#85bc3a'}]}>
              Reached PickUp Location
            </Text>
            <View style={styles.iconBox}>
              <Icon name={'check'} color="#fff" size={20} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              setRateModal(true);
            }}
            style={[styles.buttonBox, {backgroundColor: '#85bc3a'}]}>
            <Text style={styles.buttonText}>Picked Up</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View style={styles.ButtonContainer}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.buttonBox,
              {
                backgroundColor: '#fff',
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderColor: '#85bc3a',
                borderWidth: 1,
              },
            ]}>
            <Text style={[styles.buttonText, {color: '#85bc3a'}]}>
              Reached PickUp Location
            </Text>
            <View style={styles.iconBox}>
              <Icon name={'check'} color="#fff" size={20} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.buttonBox,
              {
                backgroundColor: '#85bc3a',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderColor: '#85bc3a',
                borderWidth: 1,
                backgroundColor: '#fff',
              },
            ]}>
            <Text style={[styles.buttonText, {color: '#85bc3a'}]}>
              Picked Up
            </Text>
            <View style={styles.iconBox}>
              <Icon name={'check'} color="#fff" size={20} />
            </View>
          </TouchableOpacity>
        </View>
      )}

      <Modal
        onBackButtonPress={() => {
          setModalVisible(false);
        }}
        onBackdropPress={() => {
          setModalVisible(false);
        }}
        isVisible={modalVisible}
        backdropOpacity={0.7}
        style={{
          margin: 0,
          screenBackgroundColor: 'transparent',
          modalPresentationStyle: 'overCurrentContext',
          justifyContent: 'flex-end',
        }}
        propagateSwipe={true}
        useNativeDriver={false}
        onSwipeComplete={() => setModalVisible(false)}
        swipeDirection={['down']}
        animationOut={'fadeOutDown'}
        animationInTiming={500}
        animationOutTiming={500}>
        <PickUpLocated
          item={props.items}
          orderDetails={props.orderDetails}
          setModalVisible={setModalVisible}
          setIsPickup={setIsPickup}
          reload={props.reload}
          setReload={props.setReload}
        />
      </Modal>
      <Modal
        onBackButtonPress={() => {
          setRateModal(false);
        }}
        onBackdropPress={() => {
          setRateModal(false);
        }}
        isVisible={rateModal}
        backdropOpacity={0.7}
        style={{
          margin: 0,
          screenBackgroundColor: 'transparent',
          modalPresentationStyle: 'overCurrentContext',
          justifyContent: 'flex-end',
        }}
        propagateSwipe={true}
        useNativeDriver={false}
        onSwipeComplete={() => setRateModal(false)}
        swipeDirection={['down']}
        animationOut={'fadeOutDown'}
        animationInTiming={500}
        animationOutTiming={500}>
        <RatePickup
          shop_name={props.items.shop}
          dealer_id={props.items.dealer}
          order_pk={props.order_pk}
          setIsRated={setIsRated}
          setRateModal={setRateModal}
          reload={props.reload}
          setReload={props.setReload}
        />
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  mainHead: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
    color: '#a8a8a8',
  },
  Items: {
    fontFamily: 'Poppins-Regular',
  },
  total: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
  },
  buttonText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
    color: '#fff',
  },
  dropBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: width * 0.8,
    borderBottomWidth: 1,
    borderBottomColor: '#aaa',
    backgroundColor: '#f5f5f5',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  flexBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: width * 0.8,
    paddingHorizontal: 10,
  },
  iconBox: {
    width: 25,
    height: 25,
    backgroundColor: '#85bc3a',
    borderRadius: 30,
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonBox: {
    width: width * 0.8,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    marginBottom: 15,
  },
  ButtonContainer: {
    width: width * 0.8,
    marginTop: 20,
  },
});

export default PickUpItem;
