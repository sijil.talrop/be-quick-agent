import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Image,
  Linking,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import Modal from 'react-native-modal';
const {width, height} = Dimensions.get('window');
import DeliveryLocated from './DeliveryLocated';

export default function AmountHandOver(props) {
  return (
    <View style={styles.backgroundImage}>
      <View style={[styles.contentContainer]}>
        <View style={{alignItems: 'center'}}>
          <Text style={styles.headerText}>Delivery Located</Text>
        </View>
        <View style={styles.handoverBox}>
          <View>
            <Text style={styles.leftContent}>
              {props.orderDetails.customer_details.name}
            </Text>
            <Text style={[styles.leftContent, {color: '#000'}]}>
              {props.orderDetails.customer_details.phone}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(
                `tel:${props.orderDetails.customer_details.phone}}`,
              );
            }}
            activeOpacity={0.8}
            style={styles.rightBox}>
            <Icon name={'phone'} color="#fff" size={20} />
            <Text style={styles.rightContent}>Call</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => {
            props.setLocated_modal(true);
            props.setModalVisible(false);
          }}
          style={styles.loginButton}
          activeOpacity={0.7}>
          <Text style={styles.loginText}>Amount Hands On</Text>
          <Text style={[styles.loginText, {fontSize: RFValue(15, height)}]}>
            {props.orderDetails.amount_payable}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  contentContainer: {
    width: width,
    paddingHorizontal: 30,
    paddingVertical: 32,
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 1,
    borderColor: '#eee',
  },
  loginButton: {
    backgroundColor: '#85bc3a',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    paddingVertical: 10,
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    fontSize: RFValue(15, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
  },
  handoverBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginVertical: 10,
    alignItems: 'center',
    borderStyle: 'dashed',
    borderColor: '#aaa',
    borderWidth: 1,
    borderRadius: 10,
    paddingVertical: 16,
  },
  leftContent: {
    fontFamily: 'Poppins-SemiBold',
    color: '#9c9c9c',
    fontSize: RFValue(13, height),
  },
  rightContent: {
    fontSize: RFValue(13, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#fff',
  },
  rightBox: {
    width: width * 0.2,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderRadius: 10,
    backgroundColor: '#f57952',
  },

  loginText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
    color: '#fff',
  },
});
