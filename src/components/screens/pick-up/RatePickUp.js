import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
} from 'react-native';
import {BASE_URL} from '../../../../Settings';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
const {width, height} = Dimensions.get('window');
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function RatePickUp(props) {
  const [prepared_on_time, setPrepared_on_time] = useState();
  const [packed_well, setPacked_well] = useState();
  const [staff_behaviour, setStaff_behaviour] = useState();

  const FirstLike = value => {};

  const ratePickup = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'delivery-agents/pickup-rating/';
    console.warn(post_url);
    axios
      .post(
        post_url,
        {
          dealer_id: props.dealer_id,
          order_pk: props.order_pk,
          prepared_on_time: prepared_on_time,
          packed_well: packed_well,
          staff_behaviour: staff_behaviour,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          props.setIsRated(true);
          props.setRateModal(false);
        } else {
          console.log(response.data);
        }
      })
      .catch(error => {
        console.log(error.response);
      });
  };

  return (
    <View style={styles.backgroundImage}>
      <View style={[styles.contentContainer]}>
        <View style={{alignItems: 'center'}}>
          <Text style={styles.headerText}>Rate Pickup</Text>
          <Text style={styles.subText}>{props.shop_name}</Text>
        </View>
        <View style={styles.mainFlex}>
          <Text style={styles.content}>Order Prepared on time</Text>
          <View style={styles.likeBox}>
            <TouchableOpacity
              onPress={() => {
                setPrepared_on_time('like');
              }}
              style={[
                styles.imageBox,
                {
                  backgroundColor:
                    prepared_on_time === 'like' ? '#82b838' : '#dadada',
                },
              ]}>
              <Icon name={'thumb-up'} color="#fff" size={20} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setPrepared_on_time('dislike');
              }}
              style={[
                styles.imageBox,
                {
                  backgroundColor:
                    prepared_on_time === 'dislike' ? '#82b838' : '#dadada',
                },
              ]}>
              <Icon name={'thumb-down'} color="#fff" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={[
            styles.mainFlex,
            {
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: '#eee',
            },
          ]}>
          <Text style={styles.content}>Order Packed well.</Text>
          <View style={styles.likeBox}>
            <TouchableOpacity
              onPress={() => {
                setPacked_well('like');
              }}
              style={[
                styles.imageBox,
                {
                  backgroundColor:
                    packed_well === 'like' ? '#82b838' : '#dadada',
                },
              ]}>
              <Icon name={'thumb-up'} color="#fff" size={20} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setPacked_well('dislike');
              }}
              style={[
                styles.imageBox,
                {
                  backgroundColor:
                    packed_well === 'dislike' ? '#82b838' : '#dadada',
                },
              ]}>
              <Icon name={'thumb-down'} color="#fff" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.mainFlex}>
          <Text style={styles.content}>staff Behaviour</Text>
          <View style={styles.likeBox}>
            <TouchableOpacity
              onPress={() => {
                setStaff_behaviour('like');
              }}
              style={[
                styles.imageBox,
                {
                  backgroundColor:
                    staff_behaviour === 'like' ? '#82b838' : '#dadada',
                },
              ]}>
              <Icon name={'thumb-up'} color="#fff" size={20} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setStaff_behaviour('dislike');
              }}
              style={[
                styles.imageBox,
                {
                  backgroundColor:
                    staff_behaviour === 'dislike' ? '#82b838' : '#dadada',
                },
              ]}>
              <Icon name={'thumb-down'} color="#fff" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            ratePickup();
          }}
          style={styles.loginButton}>
          <Text style={styles.loginText}>Submit</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  contentContainer: {
    width: width,
    paddingHorizontal: 30,
    paddingVertical: 32,
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 1,
    borderColor: '#eee',
  },
  loginButton: {
    backgroundColor: '#85bc3a',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    paddingVertical: 10,
  },
  headerText: {
    fontSize: RFValue(15, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
  },
  subText: {
    fontFamily: 'Poppins-Regular',
    color: '#aaa',
    fontSize: RFValue(13, height),
  },
  content: {
    fontSize: RFValue(13, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
  },
  likeBox: {
    flexDirection: 'row',
    width: width * 0.3,
    justifyContent: 'space-between',
  },
  mainFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
  imageBox: {
    width: 35,
    height: 35,
    backgroundColor: 'red',
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginText: {
    textAlign: 'center',
    fontSize: RFValue(14, height),
    color: '#fff',
    fontFamily: 'Poppins-SemiBold',
  },
  likeButton: {
    width: 70,
    height: 15,
    backgroundColor: 'red',
  },
});
