import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Keyboard,
} from 'react-native';
import {BASE_URL} from '../../../../Settings';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import Toast from 'react-native-simple-toast';
const {width, height} = Dimensions.get('window');
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LottieView from 'lottie-react-native';

export default function DeliveryLocated(props) {
  const navigation = useNavigation();
  const [received, setReceived] = useState(0);
  const [balance, setBalance] = useState(0);
  const [balanceShow, setBalanceShow] = useState(false);
  const [done, setDone] = useState(false);

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    console.log(received, parseFloat(props.orderDetails.amount_payable));
    if (received > parseFloat(props.orderDetails.amount_payable)) {
      console.log('greater');
      setBalanceShow(true);
      setDone(true);
      setBalance(
        Math.round(
          (received - parseFloat(props.orderDetails.amount_payable)) * 100,
        ) / 100,
      );
    } else if (received == parseFloat(props.orderDetails.amount_payable)) {
      console.log('equal');
      setDone(true);
    } else {
      console.log('small');
      setBalanceShow(false);
      setDone(false);
    }
  }, [received]);

  useEffect(() => {
    console.log(balance);
  }, [balance]);

  const deliveryStatus = async data => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'delivery-agents/delivery-status/';
    console.warn(post_url);
    axios
      .post(
        post_url,
        {
          delivery_status: data,
          order_pk: props.orderDetails.id,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          props.setLocated_modal(false);
          navigation.navigate('SuccessPopup');
          setLoading(false);
        } else {
          console.log(response.data);
          setLoading(false);
        }
      })
      .catch(error => {
        console.log(error);
        setLoading(false);
      });
  };

  const transferToCustomerWallet = async data => {
    console.log(received, balance);
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url =
      BASE_URL + 'delivery-agents/transfer-balance-to-customer-wallet/';
    console.warn(post_url);
    axios
      .post(
        post_url,
        {
          order_pk: props.orderDetails.id,
          received_amount: received,
          balance: String(balance),
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          props.setLocated_modal(false);
          navigation.navigate('SuccessPopup');
          setLoading(false);
        } else {
          console.log(response.data);
          Toast.show(response.data.data.message, Toast.SHORT);
          setLoading(false);
        }
      })
      .catch(error => {
        console.log(error.response);
        setLoading(false);
      });
  };

  return (
    <View style={styles.backgroundImage}>
      <View style={[styles.contentContainer]}>
        <View style={{alignItems: 'center'}}>
          <Text style={styles.headerText}>Delivery Located</Text>
        </View>
        <View>
          <View style={styles.handoverBox}>
            <Text style={styles.leftContent}>Amount Handover : </Text>
            <Text style={styles.rightContent}>
              ₹{props.orderDetails.amount_payable}
            </Text>
          </View>
          <View style={styles.middleContent}>
            <View style={styles.receivedBox}>
              <Text style={styles.leftContent}>Cash Received</Text>
              <View style={styles.rightBox}>
                <Text style={styles.rightContent}>₹</Text>
                <TextInput
                  value={received}
                  keyboardType="numeric"
                  onChangeText={text => {
                    setReceived(text);
                  }}
                  style={styles.input}
                />
              </View>
            </View>
            {balanceShow && (
              <View
                style={[
                  styles.handoverBox,
                  {backgroundColor: '#f0f9e5', paddingVertical: 10},
                ]}>
                <Text style={[styles.leftContent, {color: '#000'}]}>
                  Balance Amount
                </Text>
                <View>
                  <Text style={styles.rightContent}>₹ {balance}</Text>
                </View>
              </View>
            )}
          </View>
        </View>
        {balanceShow ? (
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              transferToCustomerWallet();
              Keyboard.dismiss();
              setLoading(true);
            }}
            style={[styles.loginButton, {paddingVertical: loading ? 0 : 10}]}>
            {loading ? (
              <LottieView
                source={require('../../../assets/lotties/loading.json')}
                autoPlay
                style={{height: 50, width: 50}}
                loop
              />
            ) : (
              <Text style={styles.loginText}>
                Transfer Bal to customer wallet
              </Text>
            )}

            <View style={styles.buttonImageContent}>
              <Text style={[styles.loginText, {fontSize: 20, marginRight: 10}]}>
                ₹ {balance}
              </Text>
              <Image
                style={{width: width * 0.05, height: width * 0.05}}
                source={require('../../../assets/icons1/handover_cash.png')}
              />
            </View>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            onPress={() => {
              if (done) {
                deliveryStatus('delivered');
                console.warn('done');
                setLoading(true);
                Keyboard.dismiss();
              } else {
                Toast.show('Please enter received amount', Toast.SHORT);
              }
            }}
            activeOpacity={0.7}
            style={[
              styles.loginButton,
              {
                backgroundColor: !done ? '#e6e6e6' : '#85bc3a',
                paddingVertical: loading ? 0 : 10,
              },
            ]}>
            {loading ? (
              <LottieView
                source={require('../../../assets/lotties/loading.json')}
                autoPlay
                style={{height: 50, width: 50}}
                loop
              />
            ) : (
              <Text style={styles.loginText}>Done</Text>
            )}
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  contentContainer: {
    width: width,
    paddingHorizontal: 30,
    paddingVertical: 32,
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 1,
    elevation: 1,
    borderColor: '#eee',
  },
  loginButton: {
    backgroundColor: '#85bc3a',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,

    paddingHorizontal: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  headerText: {
    fontSize: RFValue(15, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
  },
  handoverBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginVertical: 15,
    alignItems: 'center',
    backgroundColor: '#f9f9f9',
    padding: 10,
    borderRadius: 10,
  },
  receivedBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginVertical: 15,
    alignItems: 'center',
  },
  middleContent: {
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 10,
    // paddingHorizontal: 20,
  },
  leftContent: {
    fontFamily: 'Poppins-Regular',
    color: '#9c9c9c',
    fontSize: RFValue(12, height),
  },
  rightContent: {
    fontSize: RFValue(15, height),
    fontFamily: 'Poppins-SemiBold',
  },
  rightBox: {
    borderWidth: 0.5,
    borderColor: '#dfdfdf',
    backgroundColor: '#f9f9f9',
    minWidth: width * 0.2,
    paddingHorizontal: 10,
    alignItems: 'center',
    // justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: 10,
  },
  buttonImageContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  loginText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
    color: '#fff',
  },
  input: {
    fontSize: RFValue(14, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
    minWidth: width * 0.2,
  },
});
