import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  UIManager,
  LayoutAnimation,
  Platform,
} from 'react-native';
import {BASE_URL} from '../../../../Settings';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import posed from 'react-native-pose';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const {width, height} = Dimensions.get('window');

export default function PickUpLocated(props) {
  const [isOpen, setIsOpen] = useState(false);

  if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  const pickupDetails = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'delivery-agents/pickup-status/';
    console.warn(post_url);
    axios
      .post(
        post_url,
        {
          pickup_status: 'picked_up',
          pickup_pk: props.item.id,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          props.setReload(!props.reload);
          props.setIsPickup(true);
          props.setModalVisible(false);
        } else {
          console.log(response.data);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <View style={styles.backgroundImage}>
      <View style={[styles.contentContainer]}>
        <View style={{alignItems: 'center'}}>
          <Text style={styles.headerText}>Pickup Located</Text>
          <View style={styles.topContainer}>
            <View style={styles.thirdBox}>
              <Text style={styles.idLetter}>
                Order ID :
                <Text style={styles.idMiddleText}>
                  {' '}
                  ORD{' '}
                  <Text style={styles.idNumber}>
                    {props.orderDetails.order_id.slice(
                      props.orderDetails.order_id.length - 4,
                    )}
                  </Text>
                </Text>
              </Text>
            </View>
          </View>
          <TouchableOpacity
            onPress={() => {
              setIsOpen(!isOpen);
              LayoutAnimation.configureNext({
                duration: 500,
                update: {
                  type: LayoutAnimation.Types.spring,
                  springDamping: 0.7,
                },
              });
            }}
            style={styles.dropBox}>
            <Text style={styles.mainHead}>Order Details</Text>
            <Icon
              name={isOpen ? 'chevron-up' : 'chevron-down'}
              color="#000"
              size={30}
            />
          </TouchableOpacity>
          {props.item.order_items.map((item, index) => (
            <View
              key={index}
              style={{
                paddingVertical: isOpen ? 10 : 0,
                backgroundColor: '#f5f5f5',
              }}>
              {isOpen && (
                <React.Fragment>
                  <View style={styles.flexBox}>
                    <View style={{width: '60%'}}>
                      <Text style={styles.Items}>
                        {item.product_name} * {Math.trunc(item.qty)}
                      </Text>
                      {/* <Text style={styles.Items}>({item.weight}g)</Text> */}
                    </View>
                    <Text style={styles.Items}> ₹{item.subtotal}</Text>
                  </View>
                </React.Fragment>
              )}
            </View>
          ))}
          <View
            style={[
              styles.flexBox,
              {
                borderTopWidth: 1,
                borderTopColor: '#aaa',
                paddingVertical: 10,
              },
            ]}>
            <Text style={styles.total}>Grand Total</Text>
            <Text style={[styles.total, {fontSize: RFValue(15, height)}]}>
              {props.item.subtotal}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            pickupDetails();
          }}
          style={styles.loginButton}>
          <Text style={styles.loginText}>Order Picked</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  contentContainer: {
    width: width,
    paddingHorizontal: 30,
    paddingVertical: 32,
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 1,
    borderColor: '#eee',
  },
  loginButton: {
    backgroundColor: '#85bc3a',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    paddingVertical: 10,
  },
  headerText: {
    fontSize: RFValue(15, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: RFValue(13, height),
    fontFamily: 'Poppins-SemiBold',
  },
  loginText: {
    textAlign: 'center',
    fontSize: RFValue(14, height),
    color: '#fff',
    fontFamily: 'Poppins-SemiBold',
  },
  mainHead: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  flexBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: width * 0.8,
    backgroundColor: '#f5f5f5',
    paddingHorizontal: 10,
    // paddingVertical: 10,
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
  },
  Items: {
    fontFamily: 'Poppins-Regular',
  },
  total: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  buttonText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
    color: '#fff',
  },
  dropBox: {
    flexDirection: 'row',
    paddingVertical: 10,
    justifyContent: 'space-between',
    width: width * 0.8,
    borderBottomWidth: 1,
    borderBottomColor: '#aaa',
    backgroundColor: '#f5f5f5',
    paddingHorizontal: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  total: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  topContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f57952',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    marginVertical: 15,
    paddingVertical: 15,
    width: width * 0.9,
  },
  idLetter: {
    fontFamily: 'Poppins-Regular',
    color: '#fff',
    fontSize: RFValue(13, height),
  },
  idMiddleText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  idNumber: {
    backgroundColor: '#b3f9aa',
    fontSize: RFValue(13, height),
    fontFamily: 'Poppins-SemiBold',
  },
});
