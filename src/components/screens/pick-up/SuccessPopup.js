import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import posed from 'react-native-pose';
import {useNavigation} from '@react-navigation/native';
const {width, height} = Dimensions.get('window');
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
const transition = {type: 'tween', stiffness: 100, damping: 15};
const Wrapper = posed.View({
  big: {scale: 1, rotate: '360deg', transition},
  // small: {scale: 0.1, rotate: '0deg', transition},
});

export default function SuccessPopup() {
  const navigation = useNavigation();
  const [isBig, setIsBig] = React.useState(false);

  useEffect(() => {
    setIsBig(!isBig);
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.backgroundImage}>
        <View style={[styles.contentContainer]}>
          <View style={styles.handoverBox}>
            <View style={styles.topBox}>
              <Wrapper pose={isBig ? 'big' : 'small'}>
                <View style={styles.imageBox}>
                  <Image
                    style={styles.image}
                    source={require('../../../assets/icons1/checked.png')}
                  />
                </View>
              </Wrapper>
              <Text style={styles.successText}>Successfully Delivered</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('OnDutyPage');
              }}
              style={styles.loginButton}>
              <Text style={styles.loginText}>Go to home</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    minHeight: '100%',
    backgroundColor: 'transparent',
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
  },
  contentContainer: {
    paddingHorizontal: 30,
    backgroundColor: 'transparent',
  },
  topBox: {
    alignItems: 'center',
  },
  imageBox: {
    width: width * 0.25,
    height: width * 0.25,
    marginBottom: 20,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  loginButton: {
    backgroundColor: '#404040',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  handoverBox: {
    justifyContent: 'space-between',
    borderRadius: 20,
    height: width * 0.7,
    backgroundColor: '#fff',
    padding: 30,
    borderWidth: 1,
    elevation: 1,
    borderColor: '#eee',
  },
  loginText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
    color: '#fff',
    textAlign: 'center',
  },
  successText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
    color: '#404040',
    textAlign: 'center',
  },
});
