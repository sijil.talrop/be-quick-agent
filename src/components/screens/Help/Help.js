import React, {useRef, useEffect, useState, useContext} from 'react';
import {
  StyleSheet,
  View,
  Animated,
  Dimensions,
  Text,
  Image,
  TouchableOpacity,
  Alert,
  ImageBackground,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import BasicHeading from '../heading/BasicHeading';
import {BASE_URL} from '../../../../Settings';
import Faq from './Faq';
import Icon from 'react-native-vector-icons/FontAwesome';
const {width, height} = Dimensions.get('window');
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/native';
import VideoPlayer from 'react-native-video-controls';

export default function Help() {
  const navigation = useNavigation();
  const [isLoading, setLoading] = useState();
  const [helps, setHelps] = useState([]);
  const [faqs, setFaqs] = useState([]);

  useEffect(() => {
    helps_faq();
  }, []);

  const helps_faq = async () => {
    setLoading(true);
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let get_url = BASE_URL + 'delivery-agents/delivery-agent-help/';
    console.log(get_url);
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          console.log(response.data);
          setFaqs(data.faqs);
          setHelps(data.helps);
          setLoading(false);
        } else {
          console.log('no data');
          setLoading(false);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../../../assets/images/Group20260.png')}
        style={styles.backgroundImage}></ImageBackground>
      <View style={styles.mainContentBox}>
        <View>
          <BasicHeading title="Help?" />
        </View>
        {isLoading === false ? (
          <View>
            <ScrollView style={styles.body}>
              <View style={styles.topBox}>
                {helps.map((data, index) => (
                  <View>
                    <Text style={styles.headText}>{data.title}</Text>
                    <View style={styles.videoContainer}>
                      <VideoPlayer
                        source={{
                          uri: data.video,
                        }}
                        style={styles.mediaPlayer}
                        disableFullscreen={true}
                        disableBack={true}
                        seekColor="#f7f7f7"
                        disableVolume={true}
                      />
                    </View>
                  </View>
                ))}
              </View>
              <View>
                {faqs.map((data, index) => (
                  <Faq style={styles.faq} data={data} index={index} />
                ))}
                <View style={{height: height * 0.15}} />
              </View>
            </ScrollView>
          </View>
        ) : (
          <View
            style={{
              height: height * 0.85,
              alignItems: 'center',
              justifyContent: 'center',
              // backgroundColor: '#fff',
            }}>
            <ActivityIndicator size={30} color="#8ec641" />
          </View>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  backgroundImage: {
    flex: 1,
    // justifyContent: 'flex-end',
    resizeMode: 'cover',
    // justifyContent: 'flex-end',
    width: width * 0.4,
    height: height * 0.3,
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: -1,
  },
  mainContentBox: {
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  boxContainer: {
    flexDirection: 'column',
    paddingTop: 30,
  },
  box: {
    marginBottom: 10,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 10,
  },
  content: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  faq: {
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 10,
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  faqContent: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
    color: '#f0784f',
    marginLeft: 10,
  },

  body: {
    // width: width,
    // borderRadius: 20,
    // marginTop: 20,
    // backgroundColor: '#fff',
    // paddingHorizontal: 15,
    minHeight: '100%',
    paddingTop: 20,
  },
  headText: {
    color: '#000',
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
    marginVertical: 5,
    marginLeft: 10,
  },
  videoContainer: {
    flex: 1,
    height: 200,
    // backgroundColor: 'red',
    position: 'relative',
    paddingHorizontal: 10,
    marginBottom: 5,
  },
  mediaPlayer: {
    // backgroundColor: 'blue',
    justifyContent: 'center',
    height: 200,
    width: '100%',
    borderRadius: 8,
  },
  overlay: {
    backgroundColor: '#0006',
    height: 200,
    width: '100%',
    borderRadius: 8,
    position: 'absolute',
  },
  playIcon: {
    justifyContent: 'center',
    backgroundColor: '#5a534c',
    alignItems: 'center',
    // flexDirection: 'column',
    position: 'absolute',
    left: '45%',
    right: 0,
    top: '45%',
    borderRadius: 50,
    height: 35,
    width: 35,
    // margin: 'auto',
    // height: '100%',
  },
  faq: {},
  topBox: {
    marginBottom: 10,
    // backgroundColor: '#fff',
    borderRadius: 20,
  },
});
