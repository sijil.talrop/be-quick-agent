import React, {useState, useEffect, useRef, useContext} from 'react';
const {width, height} = Dimensions.get('window');

import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Switch,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

import {useNavigation} from '@react-navigation/native';
const Reward_Box_Height = width * 0.3;
const card_width = width * 0.9;

const Faq = props => {
  const [active, setActive] = useState(false);

  const toggleArrow = () => {
    setActive(!active);
  };
  return (
    <View
      style={[
        styles.mainContainer,
        {
          backgroundColor: active === false ? '#fff' : '#f9f9f9',
          marginBottom: active === true ? 10 : 0,
        },
      ]}>
      <TouchableOpacity
        onPress={() => {
          toggleArrow();
        }}
        style={styles.headingView}>
        <Text style={styles.question}>
          {props.index + 1}. {props.data.question}
        </Text>
        <View>
          <Icon
            name="keyboard-arrow-down"
            size={25}
            color="#7b7b7b"
            style={{
              transform: [{rotate: active === false ? '0deg' : '180deg'}],
            }}
          />
        </View>
      </TouchableOpacity>
      {active === true && (
        <View style={styles.answersView}>
          <Text style={styles.answers}>{props.data.answer}</Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    paddingTop: 10,
    // paddingBottom: 15,
    backgroundColor: '#f9f9f9',
    padding: 10,
    borderRadius: 8,
    // height: '100%',
    // justifyContent: 'space-between',
    // marginBottom: 10,
  },
  headingView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: '#ebebeb',
    borderBottomWidth: 1,
    // marginBottom: 8,
    // backgroundColor: 'red',
    paddingBottom: 8,
  },
  question: {
    color: '#000',
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  answersView: {},
  answers: {
    color: '#828282',
    fontSize: RFValue(12, height),
    fontFamily: 'Poppins-Medium',
    marginTop: 8,
    // transform: [{rotate: '90deg'}],
  },
});

export default Faq;
