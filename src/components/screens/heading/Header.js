import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const {height, width} = Dimensions.get('window');
import {useNavigation} from '@react-navigation/native';

export default function Header(props) {
  const navigation = useNavigation();

  return (
    <>
      {/* <SafeAreaView> */}
      <View style={styles.iconBox}>
        <TouchableOpacity
          onPress={() => {
            navigation.openDrawer();
          }}
          activeOpacity={8}
          pressDelay={0.4}
          style={styles.menu}>
          <Image
            style={styles.image}
            source={require('../../../assets/icons/menu.png')}
          />
        </TouchableOpacity>
        <Text style={styles.headText}>{props.title}</Text>
        <TouchableOpacity activeOpacity={8} style={styles.notification}>
          <Image
            style={styles.image}
            source={require('../../../assets/icons/delete.png')}
          />
        </TouchableOpacity>
      </View>
      {/* </SafeAreaView> */}
    </>
  );
}

const styles = StyleSheet.create({
  iconBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  menu: {
    width: 25,
    height: 25,
  },
  notification: {
    width: 25,
    height: 25,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
  },
  headText: {
    fontFamily: 'Poppins-SemiBold',
  },
  contentScroll: {},
});
