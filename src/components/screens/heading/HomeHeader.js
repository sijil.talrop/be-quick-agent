import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
const {height, width} = Dimensions.get('window');
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

export default function Header(props) {
  return (
    // <View style={styles.topView}>
    <View style={styles.iconBox}>
      <TouchableOpacity activeOpacity={8} pressDelay={0.4} style={styles.menu}>
        <Image
          style={styles.image}
          source={require('../../../assets/icons/menu.png')}
        />
      </TouchableOpacity>
      <View style={styles.logoContainer}>
        <View style={styles.logo}>
          <Image
            style={styles.image}
            source={require('../../../assets/icons/green.png')}
          />
        </View>
      </View>
      <TouchableOpacity activeOpacity={8} style={styles.notification}>
        <Image
          style={styles.image}
          source={require('../../../assets/icons/notification.png')}
        />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  iconBox: {
    height: width * 0.2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headText: {
    marginLeft: '35%',
    fontSize: RFValue(15, height),
    color: '#000',
  },
  menu: {
    width: 30,
    height: 30,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
  },
  logoContainer: {
    width: 60,
    height: 60,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
  },
  logo: {
    width: 50,
    height: 50,
    backgroundColor: '#f6f6f6',
  },
  notification: {
    width: 30,
    height: 30,
  },
});
