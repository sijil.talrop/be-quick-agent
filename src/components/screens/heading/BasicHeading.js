import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
} from 'react-native';
import {Left, Right, Body} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
const {height, width} = Dimensions.get('window');
import {useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

export default function BasicHeadings(props) {
  const navigation = useNavigation();
  useEffect(() => {
    console.warn(props.title);
  }, []);
  return (
    <View style={styles.iconBox}>
      <Left style={{flex: 1}}>
        <TouchableOpacity activeOpacity={8} style={styles.menu}>
          <Icon
            name="arrow-back"
            onPress={() => {
              navigation.goBack();
            }}
            size={24}
            color={'#000'}
          />
        </TouchableOpacity>
      </Left>
      <Body style={{flex: 4}}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            flex: 2,
          }}>
          <Text style={styles.headText}>{props.title}</Text>
        </View>
      </Body>
      <Right style={{flex: 1}}></Right>
    </View>
  );
}

const styles = StyleSheet.create({
  iconBox: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    backgroundColor: 'transparent',
  },
  headText: {
    fontSize: RFValue(15, height),
    color: '#000',
    fontFamily: 'Poppins-SemiBold',
  },
  menu: {
    width: 25,
    height: 25,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
  },
});
