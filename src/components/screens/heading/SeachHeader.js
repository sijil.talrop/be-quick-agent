import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  StatusBar,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
const {height, width} = Dimensions.get('window');

export default function Header(props) {
  return (
    // <View style={styles.topView}>
    <>
      <SafeAreaView>
        <View style={styles.iconBox}>
          <TouchableOpacity
            activeOpacity={8}
            pressDelay={0.4}
            style={styles.menu}>
            <Image
              style={styles.image}
              source={require('../../../assets/icons/menu.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={8} style={styles.notification}>
            <Image
              style={styles.image}
              source={require('../../../assets/icons/after.png')}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  iconBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  menu: {
    width: 25,
    height: 25,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
  },
  notification: {
    width: 25,
    height: 25,
  },
});
