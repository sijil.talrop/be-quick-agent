import React, {useEffect, useState, useCallback, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ImageBackground,
  TouchableWithoutFeedback,
  Switch,
  SafeAreaView,
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  Animated,
  Easing,
  BackHandler,
  Alert,
  ActivityIndicator,
} from 'react-native';
// import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
var {height, width} = Dimensions.get('window');
const thirdBoxWidth = width * 0.4;
import {UserContext} from '../../../contexts/stores/UserStore';
import {BASE_URL} from '../../../../Settings';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import OrderItem from './OrderItem';
import RefreshPage from './RefreshPage';
import Menu from '../menu/Menu';
import Toast from 'react-native-simple-toast';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import axios from 'axios';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MenuIcon from '../../../assets/icons1/Menu.svg';

const OnDutyPage = ({route}) => {
  const {userState, userDispatch} = useContext(UserContext);
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const [switchValue, setSwitchValue] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [orders, setOrders] = useState([]);
  const [reload, setReload] = useState(false);
  const [loading, setLoading] = useState(true);
  const [menuModal, setMenuModal] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const toggleSwitch = value => {
    setSwitchValue(value);
  };

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      getOrders();
      get_profile();
      setRefreshing(false);
    }, 500);
  });

  const onLongRefresh = useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      getOrders();
      get_profile();
      setRefreshing(false);
    }, 2000);
  });

  const [spinAnim, setSpinAnim] = useState(new Animated.Value(0));
  const spin = spinAnim.interpolate({
    inputRange: [0, 1],
    outputRange: ['1deg', '360deg'],
  });

  const Rotation = () => {
    Animated.loop(
      Animated.timing(spinAnim, {
        toValue: 1,
        duration: 3000,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
    ).start();
  };

  useEffect(() => {
    Rotation();
  });

  useEffect(() => {
    getOrders();
    get_profile();
  }, [reload]);

  useEffect(() => {
    if (isFocused) {
      getOrders();
      get_profile();
    }
  }, [isFocused]);

  useEffect(() => {
    console.log(userState.device_id, 'kittiiiiiiii moneeeeeee');
    fcmDevice();
  }, [userState.device_id]);

  const fcmDevice = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'users/create-device/';
    console.warn(post_url);
    axios
      .post(
        post_url,
        {
          registration_id: userState.device_id,
          type: 'android',
          name: 'delivery agent',
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode} = response.data;
        if (StatusCode == 6000) {
          console.log(response.data);
        } else {
          console.log(response.data);
        }
      })
      .catch(error => {
        console.log(error.response);
      });
  };

  const getOrders = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let get_url = BASE_URL + 'orders/view/';
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          setTimeout(() => {
            setOrders(data);
            setLoading(false);
          }, 500);
        } else {
          console.log('no data');
        }
      })
      .catch(error => {
        console.log(error.response, '////');
      });
  };

  const get_profile = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let get_url = BASE_URL + 'delivery-agents/profile/';
    console.warn(get_url);
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then(response => {
        if (response.data.StatusCode == 6000) {
          let data = response.data.data;
          console.log(data);
          userDispatch({
            type: 'UPDATE_USER',
            user: {
              user: 'user',
              is_duty: data.is_duty,
              live_time: data.live_duty_time,
            },
          });
        } else {
          console.log('not user');
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View style={styles.container}>
          <View style={styles.firstBox}>
            <Text style={styles.topContentHead}>On Duty</Text>
            <Text style={styles.openTime}>
              (Live for {userState.live_time})
            </Text>
          </View>
          {!loading && orders.length > 0 && (
            <View style={styles.secondBox}>
              <Text style={styles.LatestCount}>
                Latest Orders({orders.length})
              </Text>
            </View>
          )}

          {!loading &&
            orders &&
            orders.length > 0 &&
            orders.map((item, index) => (
              <TouchableOpacity
                onPress={() => {
                  item.accepted_by_delivery_agent &&
                    navigation.navigate('PickUpPage', {
                      order_pk: item.id,
                    });
                }}
                activeOpacity={0.8}
                key={index}
                style={{
                  borderRadius: 10,
                  marginBottom: '10%',
                  backgroundColor: '#fff',
                  paddingBottom: 10,
                }}>
                <View>
                  <View style={styles.thirdBox}>
                    <Text style={styles.idLetter}>
                      Order ID :
                      <Text style={styles.idMiddleText}>
                        {' '}
                        ORD{' '}
                        <Text style={styles.idNumber}>
                          {item.order_id.slice(item.order_id.length - 3)}
                        </Text>
                      </Text>
                    </Text>
                    <View style={styles.rightImage}>
                      {item.delivery_method != 'Normal' && (
                        <Image
                          style={styles.imageBox}
                          source={require('../../../assets/icons1/highlated.png')}></Image>
                      )}
                    </View>
                  </View>
                  <View style={styles.fourthBox}>
                    <Text style={[styles.kmTextRow]}>
                      {item.distance_and_time.distance}, {''}
                      {item.distance_and_time.time}
                    </Text>
                    <Text style={styles.kmTextRow}>{item.date}</Text>
                  </View>
                </View>
                <View>
                  {item.pickup_locations.map((item, index) => (
                    <View style={[styles.locationBox, {marginBottom: 20}]}>
                      <View style={styles.locationImageBox}>
                        <Image
                          style={styles.locationImage}
                          source={require('../../../assets/icons1/location.png')}
                        />
                      </View>
                      <View style={{marginLeft: 20}}>
                        <Text style={styles.shopName}>{item.name}</Text>
                        <Text style={styles.kmText}>{item.location}</Text>
                      </View>
                    </View>
                  ))}
                </View>
                <OrderItem
                  ordersDetails={item.order_items}
                  grandTotal={item.amount_payable}
                  pk={item.id}
                  accepted_by_delivery_agent={item.accepted_by_delivery_agent}
                  reload={reload}
                  setReload={setReload}
                />
              </TouchableOpacity>
            ))}
          {loading && (
            <View
              style={{
                height: height * 0.4,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ActivityIndicator size={30} color="#85bc3a" />
            </View>
          )}
          {!loading && orders.length === 0 && (
            <View
              style={{
                height: height * 0.6,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <RefreshPage onLongRefresh={onLongRefresh} />
            </View>
          )}
        </View>
        <ScrollView>
          <Modal
            onBackButtonPress={() => {
              setMenuModal(false);
            }}
            onBackdropPress={() => {
              setMenuModal(false);
            }}
            isVisible={menuModal}
            backdropOpacity={0.7}
            style={{
              margin: 0,
              screenBackgroundColor: 'transparent',
              modalPresentationStyle: 'overCurrentContext',
              justifyContent: 'flex-end',
            }}
            propagateSwipe={true}
            useNativeDriver={false}
            onSwipeComplete={() => setMenuModal(false)}
            swipeDirection={['down']}
            animationOut={'fadeOutDown'}
            animationInTiming={500}
            animationOutTiming={500}>
            <Menu setMenuModal={setMenuModal} />
          </Modal>
        </ScrollView>
      </ScrollView>
      <ImageBackground
        source={require('../../../assets/images/Group20260.png')}
        style={styles.backgroundImage}></ImageBackground>
      <View style={{alignItems: 'center'}}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={() => {
            // navigation.navigate('Menu');
            setMenuModal(true);
          }}
          style={{
            position: 'absolute',
            bottom: 60,
            width: 50,
            height: 50,
            // borderWidth: 0.1,
            borderRadius: 100,
            // borderColor: '#eee',
            elevation: 3,
            backgroundColor: '#fff',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          {/* <Animated.Image
            style={{
              resizeMode: 'contain',
              width: null,
              height: null,
              flex: 1,
              // transform: [{rotate: spin}],
            }}
            source={require('../../../assets/icons1/menu.png')}
          /> */}
          <MenuIcon width={50} height={50} />
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  topContentHead: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
  },
  openTime: {
    fontFamily: 'Poppins-Regular',
    // fontSize: 15,
    fontSize: RFValue(12, height),
  },
  LatestCount: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  idLetter: {
    fontFamily: 'Poppins-LightItalic',
    fontSize: RFValue(13, height),
  },
  idMiddleText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  idNumber: {
    backgroundColor: '#b3f9aa',
  },
  kmText: {
    color: '#9c9c9c',
    fontFamily: 'Poppins-Regular',
  },
  kmTextRow: {
    color: '#9c9c9c',
    fontFamily: 'Poppins-Regular',
    backgroundColor: '#f7f7f7',
    paddingVertical: 5,
    paddingHorizontal: 10,
    fontSize: RFValue(12, height),
  },
  shopName: {
    fontSize: RFValue(13, height),
    fontFamily: 'Poppins-SemiBold',
  },
  container: {
    // backgroundColor: '#f5f5f5',
    minHeight: '100%',
    width: width,
    paddingHorizontal: 20,
    paddingTop: height * 0.07,
  },
  backgroundImage: {
    flex: 1,
    // justifyContent: 'flex-end',
    resizeMode: 'cover',
    // justifyContent: 'flex-end',
    width: width * 0.4,
    height: height * 0.3,
    position: 'absolute',
    right: 0,
    zIndex: 1,
    zIndex: -1,
  },
  firstBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: '#eee',
    backgroundColor: '#fff',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 20,
  },
  secondBox: {
    alignItems: 'center',
    paddingVertical: 15,
    borderRadius: 10,
  },
  thirdBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#eee',
    backgroundColor: '#fff',
    borderRadius: 10,
    paddingLeft: 15,
  },
  fourthBox: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 20,
    justifyContent: 'space-between',
  },
  locationBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  //
  locationImageBox: {
    width: 20,
    height: 20,
  },
  locationImage: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  rightImage: {
    width: thirdBoxWidth,
    height: thirdBoxWidth * 0.26,
  },
  imageBox: {
    width: null,
    height: null,
    flex: 1,
    overflow: 'hidden',
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
});

export default OnDutyPage;
