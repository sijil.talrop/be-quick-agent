import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  UIManager,
  LayoutAnimation,
  Platform,
} from 'react-native';
// import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
var {height, width} = Dimensions.get('window');
const thirdBoxWidth = width * 0.4;
import {BASE_URL} from '../../../../Settings';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import LottieView from 'lottie-react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import OrderDecline from '../order/OrderDecline';
import DeclineReasonPage from '../order/DeclineReasonPage';
import Modal from 'react-native-modal';

const OrderItem = ({
  ordersDetails,
  grandTotal,
  pk,
  reload,
  setReload,
  accepted_by_delivery_agent,
}) => {
  const navigation = useNavigation();
  const [switchValue, setSwitchValue] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [acceptLoading, setAcceptLoading] = useState(false);
  const toggleSwitch = value => {
    setSwitchValue(value);
  };
  const [modalVisible, setModalVisible] = useState(false);
  const [reason_modal, setReason_modal] = useState(false);

  useEffect(() => {
    console.log(ordersDetails, grandTotal);
  }, []);

  if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  // useEffect(() => {
  //   BackHandler.addEventListener('hardwareBackPress', handleBackButton);
  //   return () => {
  //     BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
  //   };
  // }, []);

  // const handleBackButton = () => {
  //   Alert.alert(
  //     'Exit App',
  //     'Are you sure you want to exit?',
  //     [
  //       {
  //         text: 'Cancel',
  //         onPress: () => console.log('Cancel Pressed'),
  //         style: 'cancel',
  //       },
  //       {
  //         text: 'OK',
  //         onPress: () => BackHandler.exitApp(),
  //       },
  //     ],
  //     {
  //       cancelable: false,
  //     },
  //   );
  //   return true;
  // };

  const acceptOrder = async () => {
    setAcceptLoading(true);
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'orders/accept-order/';
    axios
      .post(
        post_url,
        {
          pk: pk,
          order_status: true,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          setTimeout(() => {
            Toast.show(data.message, Toast.LONG);
            console.warn(StatusCode);
            setAcceptLoading(false);
            navigation.navigate('PickUpPage', {
              order_pk: pk,
            });
          }, 500);
        } else {
          setTimeout(() => {
            console.warn(StatusCode);
            Toast.show(data.message, Toast.LONG);
            setAcceptLoading(false);
          }, 500);
        }
      })
      .catch(error => {
        console.log(error.response);
        setAcceptLoading(false);
      });
  };

  return (
    <View style={{alignItems: 'center', marginTop: 20}}>
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => {
          setIsOpen(!isOpen);
          LayoutAnimation.configureNext({
            duration: 500,
            update: {
              type: LayoutAnimation.Types.spring,
              springDamping: 0.7,
            },
          });
        }}
        style={styles.dropBox}>
        <Text style={styles.mainHead}>Order Details</Text>
        <Icon
          name={isOpen ? 'chevron-up' : 'chevron-down'}
          color="#000"
          size={30}
        />
      </TouchableOpacity>
      {ordersDetails.map((item, index) => (
        <View
          key={index}
          style={{
            paddingVertical: isOpen ? 10 : 0,
            backgroundColor: '#f5f5f5',
            width: width * 0.8,
          }}>
          {isOpen && (
            <React.Fragment>
              <View style={styles.flexBox}>
                <View style={{width: '60%'}}>
                  <Text style={[styles.Items]}>
                    {item.product_name} * {Math.trunc(item.qty)}
                  </Text>
                  {/* <Text style={styles.Items}>({item.weight}g)</Text> */}
                </View>
                <Text style={styles.Items}> ₹{item.subtotal}</Text>
              </View>
            </React.Fragment>
          )}
        </View>
      ))}
      <View
        style={[
          styles.flexBox,
          {
            borderTopWidth: isOpen ? 1 : 0,
            borderTopColor: '#aaa',
            backgroundColor: '#f5f5f5',
            paddingHorizontal: 10,
            paddingVertical: 10,
            borderBottomLeftRadius: 10,
            borderBottomRightRadius: 10,
          },
        ]}>
        <Text style={styles.total}>Grand Total</Text>
        <Text style={styles.total}>₹ {grandTotal}</Text>
      </View>
      {accepted_by_delivery_agent ? (
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            navigation.navigate('PickUpPage', {order_pk: pk});
          }}
          style={[
            styles.buttonBox,
            {backgroundColor: '#85bc3a', marginTop: 20, width: width * 0.8},
          ]}>
          <Text style={styles.buttonText}>Accepted</Text>
        </TouchableOpacity>
      ) : (
        <View style={styles.ButtonContainer}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => setModalVisible(true)}
            style={[styles.buttonBox, {backgroundColor: '#d86666'}]}>
            <Icon name="close" color="#fff" size={16} />
            <Text style={styles.buttonText}>Decline</Text>
          </TouchableOpacity>
          {acceptLoading ? (
            <TouchableOpacity
              style={[styles.buttonBox, {backgroundColor: '#85bc3a'}]}>
              <LottieView
                source={require('../../../assets/lotties/loading.json')}
                autoPlay
                style={{height: 50, width: 50}}
                loop
              />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => {
                // navigation.navigate('PickUpPage');
                acceptOrder();
              }}
              style={[styles.buttonBox, {backgroundColor: '#85bc3a'}]}>
              <Icon name="check" color="#fff" size={16} />
              <Text style={styles.buttonText}>Accept</Text>
            </TouchableOpacity>
          )}
        </View>
      )}

      <Modal
        onBackButtonPress={() => {
          setModalVisible(false);
        }}
        onBackdropPress={() => {
          setModalVisible(false);
        }}
        isVisible={modalVisible}
        backdropOpacity={0.7}
        style={{
          margin: 0,
          screenBackgroundColor: 'transparent',
          modalPresentationStyle: 'overCurrentContext',
          justifyContent: 'flex-end',
        }}
        propagateSwipe={true}
        useNativeDriver={false}
        onSwipeComplete={() => setModalVisible(false)}
        swipeDirection={['down']}
        animationOut={'fadeOutDown'}
        animationInTiming={500}
        animationOutTiming={500}>
        <OrderDecline
          setReason_modal={setReason_modal}
          setModalVisible={setModalVisible}
        />
      </Modal>
      <Modal
        onBackButtonPress={() => {
          setReason_modal(false);
        }}
        onBackdropPress={() => {
          setReason_modal(false);
        }}
        isVisible={reason_modal}
        backdropOpacity={0.7}
        style={{
          margin: 0,
          screenBackgroundColor: 'transparent',
          modalPresentationStyle: 'overCurrentContext',
          justifyContent: 'flex-end',
        }}
        propagateSwipe={true}
        useNativeDriver={false}
        onSwipeComplete={() => setReason_modal(false)}
        swipeDirection={['down']}
        animationOut={'fadeOutDown'}
        animationInTiming={500}
        animationOutTiming={500}>
        <DeclineReasonPage
          setReason_modal={setReason_modal}
          pk={pk}
          reload={reload}
          setReload={setReload}
        />
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  mainHead: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  Items: {
    fontFamily: 'Poppins-Regular',
    fontSize: RFValue(13, height),
  },
  total: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  buttonText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
    color: '#fff',
  },
  dropBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: width * 0.8,
    borderBottomWidth: 1,
    borderBottomColor: '#aaa',
    backgroundColor: '#f5f5f5',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  flexBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: width * 0.8,
    paddingHorizontal: 10,
  },
  buttonBox: {
    width: width * 0.3,
    backgroundColor: 'red',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    // paddingVertical: 10,
    height: 40,
    borderRadius: 10,
  },
  ButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: width * 0.8,
    marginTop: 20,
  },
});

export default OrderItem;
