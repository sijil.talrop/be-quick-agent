import React, {useEffect, useState, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ScrollView,
  ImageBackground,
  BackHandler,
  TouchableWithoutFeedback,
  Switch,
  SafeAreaView,
} from 'react-native';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

var {height, width} = Dimensions.get('window');
import {UserContext} from '../../../contexts/stores/UserStore';
import {BASE_URL} from '../../../../Settings';
import Menu from '../menu/Menu';
import Modal from 'react-native-modal';
import {TouchableOpacity} from 'react-native-gesture-handler';
import MenuIcon from '../../../assets/icons1/Menu.svg';

const DutyOptionPage = ({route}) => {
  const {userState, userDispatch} = useContext(UserContext);
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const [sec, setSec] = useState(5);
  const [menuModal, setMenuModal] = useState(false);
  const [switchValue, setSwitchValue] = useState();

  const toggleSwitch = value => {
    setSwitchValue(value);
    // {
    //   switchValue
    //     ? navigation.navigate('DutyOptionPage')
    //     : navigation.navigate('OnDutyPage');
    // }
  };

  useEffect(() => {
    if (isFocused) {
      setSwitchValue(userState.is_duty);
    }
  }, [isFocused]);

  useEffect(() => {
    if (switchValue) {
      setSec(4);
    }
  }, [switchValue]);

  useEffect(() => {
    if (switchValue && sec > 0) {
      setTimeout(() => {
        setSec(sec - 1);
      }, 1000);
    } else if (switchValue && sec == 0) {
      setDuty();
    }
  }, [sec]);

  useEffect(() => {
    console.log(userState.device_id, 'kittiiiiiiii moneeeeeee');
    fcmDevice();
  }, [userState.device_id]);

  const setDuty = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'delivery-agents/duty-status/';
    console.warn(post_url);
    axios
      .post(
        post_url,
        {
          data: switchValue,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode} = response.data;
        if (StatusCode == 6000) {
          userDispatch({
            type: 'UPDATE_USER',
            user: {
              is_duty: switchValue,
            },
          });
          navigation.navigate('OnDutyPage');
        } else {
        }
      })
      .catch(error => {
        console.log(error.response);
      });
  };

  const fcmDevice = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'users/create-device/';
    console.warn(post_url);
    axios
      .post(
        post_url,
        {
          registration_id: userState.device_id,
          type: 'android',
          name: 'delivery agent',
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode} = response.data;
        if (StatusCode == 6000) {
          console.log(response.data);
        } else {
          console.log(response.data);
        }
      })
      .catch(error => {
        console.log(error.response);
      });
  };

  return (
    <>
      <SafeAreaView>
        <ImageBackground
          source={require('../../../assets/images/Group20260.png')}
          style={styles.container}>
          <View
            style={{
              alignItems: 'center',
              flex: 1,
              justifyContent: 'space-around',
            }}>
            <View style={switchValue ? styles.topBox : styles.offDuty}>
              <Text style={styles.optionText}>
                {switchValue ? 'On Duty ' : ' Off Duty'}
              </Text>
              {switchValue && (
                <Text style={styles.rightContent}>
                  (Live for {userState.live_time})
                </Text>
              )}
            </View>
            <View style={{alignItems: 'center'}}>
              <View style={styles.imageContainer}>
                {switchValue ? (
                  <Image
                    source={require('../../../assets/illustrations/welcome.png')}
                    style={styles.image}
                  />
                ) : (
                  <Image
                    source={require('../../../assets/illustrations/offduty.png')}
                    style={styles.image}
                  />
                )}
              </View>
              <View style={styles.ForgotBox}>
                <Text style={styles.processText}>
                  {switchValue ? 'Welcome' : ' Off Duty'}
                </Text>
                <Text style={styles.subText}>
                  {switchValue
                    ? `Let's start the day in ${sec} seconds`
                    : ' Turn on duty to work'}
                </Text>
              </View>
              <View
                style={[
                  styles.container2,
                  {backgroundColor: switchValue ? '#f57952' : '#9d9d9d'},
                ]}>
                <Text style={styles.switchText}>
                  {switchValue ? 'Duty on' : 'Duty Off'}
                </Text>
                <Switch
                  onValueChange={toggleSwitch}
                  value={switchValue}
                  trackColor={{false: '#fff', true: '#fff'}}
                  thumbColor={switchValue ? '#f57952' : '#9d9d9d'}
                  style={{transform: [{scaleX: 1.6}, {scaleY: 1.6}]}}
                />
              </View>
            </View>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => {
                // navigation.navigate('Menu');
                setMenuModal(true);
              }}
              style={styles.bottomImage}>
              {/* <Image
                source={require('../../../assets/icons1/menu.png')}
                style={styles.image1}
              /> */}
              <MenuIcon width={50} height={50} />
            </TouchableOpacity>
          </View>
        </ImageBackground>
        <ScrollView>
          <Modal
            animationInTiming={800}
            style={{margin: 0}}
            animationOutTiming={1500}
            animationType="slide"
            // swipeDirection={'down'}
            backdropColor="transparent"
            onSwipeMove={() => {
              setMenuModal(false);
            }}
            visible={menuModal}
            onBackdropPress={() => setMenuModal(false)}
            onRequestClose={() => {
              setMenuModal(!menuModal);
            }}>
            <Menu
              type="duty_option"
              setMenuModal={setMenuModal}
              setSwitchValue={setSwitchValue}
              switchValue={switchValue}
            />
          </Modal>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    alignItems: 'center',
    minHeight: '100%',
    width: width,
    paddingBottom: 30,
  },
  container2: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 30,
    paddingVertical: 10,
    width: width * 0.4,
  },
  topBox: {
    width: width * 0.9,
    backgroundColor: '#fff',
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: '#eee',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  offDuty: {
    backgroundColor: '#fff',
    width: width * 0.9,
    alignItems: 'center',
    paddingVertical: 15,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: '#eee',
  },
  imageContainer: {
    width: width * 0.3,
    height: width * 0.3,
  },
  optionText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  rightContent: {
    fontFamily: 'Poppins-Regular',
  },
  processText: {
    color: '#000',
    fontFamily: 'Poppins-SemiBold',
    textAlign: 'center',
    width: width * 0.6,
  },
  subText: {
    fontFamily: 'Poppins-Regular',
    color: '#737373',
    fontSize: RFValue(13, height),
  },
  ForgotBox: {
    alignItems: 'center',
    marginVertical: height * 0.05,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  switchText: {
    fontFamily: 'Poppins-SemiBold',
    color: '#fff',
  },
  bottomImage: {
    width: 50,
    height: 50,
    // borderWidth: 0.1,
    borderRadius: 100,
    // borderColor: '#eee',
    elevation: 3,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image1: {
    width: null,
    height: null,
    flex: 1,
    borderWidth: 2,
    overflow: 'hidden',
  },
});

export default DutyOptionPage;
