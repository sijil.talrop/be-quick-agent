import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
var {height, width} = Dimensions.get('window');
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

const RefreshPage = props => {
  return (
    <>
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
          }}>
          <View style={{alignItems: 'center'}}>
            <View style={styles.imageContainer}>
              <Image
                source={require('../../../assets/illustrations/refresh_duty.png')}
                style={styles.image}
              />
            </View>
            <View style={styles.ForgotBox}>
              <Text style={styles.processText}>Searching for new orders</Text>
            </View>
          </View>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              props.onLongRefresh();
            }}
            style={styles.bottomImage}>
            <View style={styles.imageBox}>
              <Image
                style={styles.image}
                source={require('../../../assets/icons1/refresh.png')}
              />
            </View>
            <Text style={styles.buttonFont}>Refresh</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    minHeight: '100%',
    width: width,
  },
  imageContainer: {
    width: width * 0.2,
    height: width * 0.2,
  },
  processText: {
    color: '#000',
    fontFamily: 'Poppins-SemiBold',
    textAlign: 'center',
    fontSize: RFValue(13, height),
  },
  subText: {
    fontFamily: 'Poppins-LightItalic',
    color: '#737373',
    fontSize: RFValue(12, height),
  },
  ForgotBox: {
    alignItems: 'center',
    marginTop: height * 0.04,
    marginBottom: height * 0.03,
  },
  imageBox: {
    width: 20,
    height: 20,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  bottomImage: {
    borderRadius: 10,
    borderColor: '#eee',
    backgroundColor: '#85bc3a',
    paddingVertical: 10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  buttonFont: {
    fontFamily: 'Poppins-SemiBold',
    color: '#fff',
    marginLeft: 15,
  },
});

export default RefreshPage;
