import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Animated,
  TextInput,
} from 'react-native';
const {width, height} = Dimensions.get('window');
import {BASE_URL} from '../../../../Settings';
import axios from 'axios';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';

export default function OrderDecline(props) {
  const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity);
  const bottomValue = useRef(new Animated.Value(100)).current;
  const [selectedBox, setSelectedBox] = useState('');
  const [reason, setReason] = useState('');

  const confirmHandler = value => {
    setSelectedBox(value);
  };

  useEffect(() => {
    setSelectedBox(Tab[0].Box);
    console.log(props.pk);
  }, []);

  const Tab = [
    {
      Box: 'Too long',
    },
    {
      Box: 'Time Waste',
    },
    {
      Box: 'Large order list',
    },
  ];

  const orderDecline = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'orders/accept-order/';
    console.warn(post_url, access_token, props.pk);
    axios
      .post(
        post_url,
        {
          pk: props.pk,
          order_status: false,
          reason: selectedBox,
          description: reason,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          props.setReload(!props.reload);
          props.setReason_modal(false);
          Toast.show(data.message, Toast.LONG);
        } else {
          console.log(response.data);
          Toast.show(data.message, Toast.LONG);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const renderTab = () => {
    return (
      <View style={{}}>
        <View style={styles.tabView}>
          {Tab.map((item, index) => (
            <AnimatedTouchable
              key={index}
              onPress={() => {
                confirmHandler(item.Box);
              }}
              style={[
                styles.tabBox,
                {
                  borderColor: item.Box === selectedBox ? '#85bc3a' : '#f3f5f8',
                },
              ]}>
              <Text
                style={[
                  styles.buttonText,
                  {color: item.Box == selectedBox ? '#85bc3a' : '#8d8989'},
                ]}>
                {item.Box}
              </Text>
            </AnimatedTouchable>
          ))}
        </View>
      </View>
    );
  };

  return (
    <Animated.View style={[styles.contentContainer]}>
      <View>
        <View style={{alignItems: 'center', marginBottom: 10}}>
          <Text style={styles.headerText}>Declined Reason</Text>
        </View>
        <View>{renderTab()}</View>
        <View style={styles.loginUser}>
          <TextInput
            value={reason}
            style={styles.input}
            placeholder="Write Your reason"
            returnKeyType={'done'}
            onChangeText={val => setReason(val)}
            multiline={true}
          />
        </View>
        <AnimatedTouchable
          activeOpacity={0.7}
          onPress={() => {
            // props.setReason_modal(false);
            orderDecline();
          }}
          style={[styles.loginButton]}>
          <Text style={styles.loginText}>Submit</Text>
        </AnimatedTouchable>
      </View>
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  contentContainer: {
    paddingHorizontal: 30,
    paddingVertical: '10%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 1,
    borderColor: '#eee',
  },
  loginButton: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 10,
    backgroundColor: '#85bc3a',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    fontSize: RFValue(15, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
  },
  loginText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
    color: '#fff',
  },
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingVertical: 15,
    borderRadius: 10,
    marginVertical: 20,
  },
  tabBox: {
    // width: width * 0.24,
    alignItems: 'center',
    borderRadius: 10,
    paddingVertical: 8,
    paddingHorizontal: 10,
    borderWidth: 1,
  },
  buttonText: {
    fontFamily: 'Poppins-LightItalic',
    fontSize: RFValue(12, height),
  },
  loginUser: {
    backgroundColor: '#fff',
    height: width * 0.2,
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: '#7f7c7c',
    borderRadius: 10,
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  input: {
    fontFamily: 'Poppins-LightItalic',
    fontSize: RFValue(13, height),
    color: '#7f7c7c',
  },
});
