import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Animated,
  Dimensions,
  Text,
  Image,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  UIManager,
  LayoutAnimation,
  Platform,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import posed from 'react-native-pose';
import axios from 'axios';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {BASE_URL} from '../../../../Settings';

const {width, height} = Dimensions.get('window');

export default function OrderInfoModal(props) {
  const navigation = useNavigation();
  const [isOpen, setIsOpen] = useState(false);
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getDetails();
  }, []);

  if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  const getDetails = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let get_url = BASE_URL + `orders/single-order/?order_pk=${props.pk}`;
    console.log(get_url);
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then(response => {
        if (response.data.StatusCode === 6000) {
          setTimeout(() => {
            setData(response.data.data);
            console.log(response.data.data);
            setLoading(false);
          }, 500);
        } else {
          setTimeout(() => {
            setData([]);
            setLoading(false);
          }, 500);
        }
      })
      .catch(error => {
        setTimeout(() => {
          console.log(error);
          setLoading(false);
        }, 500);
      });
  };

  return (
    <Animated.View style={[styles.contentContainer]}>
      {!loading ? (
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.thirdBox}>
            <Text style={styles.idLetter}>
              Order ID :
              <Text style={styles.idMiddleText}>
                {' '}
                ORD{' '}
                <Text style={styles.idNumber}>
                  {data.order_id.slice(data.order_id.length - 4)}
                </Text>
              </Text>
            </Text>
            <View style={styles.subTextBox}>
              <Text style={[styles.subText, {marginRight: 20}]}>
                {data.date}
              </Text>
              <Text style={styles.subText}>
                {data.distance_and_time.time},{data.distance_and_time.distance}
              </Text>
            </View>
          </View>

          <View style={{marginBottom: 20}}>
            {data.pickup_locations.map((item, index) => (
              <View style={[styles.locationBox, {marginBottom: 20}]}>
                <View style={styles.locationImageBox}>
                  <Image
                    style={styles.locationImage}
                    source={require('../../../assets/icons1/location.png')}
                  />
                </View>
                <View style={{marginLeft: 20}}>
                  <Text style={styles.shopName}>{item.name}</Text>
                  <Text style={styles.kmText}>{item.location}</Text>
                </View>
              </View>
            ))}
          </View>
          <TouchableOpacity
            onPress={() => {
              setIsOpen(!isOpen);
              LayoutAnimation.configureNext({
                duration: 500,
                update: {
                  type: LayoutAnimation.Types.spring,
                  springDamping: 0.7,
                },
              });
            }}
            style={styles.dropBox}>
            <Text style={styles.mainHead}>Order Details</Text>
            <Icon
              name={isOpen ? 'chevron-up' : 'chevron-down'}
              color="#000"
              size={25}
            />
          </TouchableOpacity>
          {data.order_items.map((item, index) => (
            <View
              key={index}
              style={{
                paddingVertical: isOpen ? 10 : 0,
                backgroundColor: '#f5f5f5',
                paddingHorizontal: 10,
              }}>
              {isOpen && (
                <React.Fragment>
                  <View style={styles.flexBox}>
                    <View style={{width: '60%'}}>
                      <Text style={styles.Items}>
                        {item.product_name} * {Math.trunc(item.qty)}
                      </Text>
                      {/* <Text style={styles.Items}>({item.weight}g)</Text> */}
                    </View>
                    <Text style={styles.Items}> ₹{item.subtotal}</Text>
                  </View>
                </React.Fragment>
              )}
            </View>
          ))}

          <View
            style={[
              styles.flexBox,
              {
                borderTopWidth: 0.5,
                borderTopColor: '#aaa',
                backgroundColor: '#f5f5f5',
                paddingHorizontal: 10,
                paddingBottom: 5,
                paddingTop: 10,
              },
            ]}>
            <Text style={styles.total}>Sub total</Text>
            <Text style={[styles.total, {fontSize: RFValue(12, height)}]}>
              {' '}
              ₹ {data.total_amount}
            </Text>
          </View>
          <View
            style={[
              styles.flexBox,
              {
                backgroundColor: '#f5f5f5',
                paddingHorizontal: 10,
                paddingBottom: 5,
              },
            ]}>
            <Text style={styles.total}>Discount</Text>
            <Text style={[styles.total, {fontSize: RFValue(12, height)}]}>
              {' '}
              ₹ {data.discount_amount}
            </Text>
          </View>
          <View
            style={[
              styles.flexBox,
              {
                backgroundColor: '#f5f5f5',
                paddingHorizontal: 10,
                paddingBottom: 5,
              },
            ]}>
            <Text style={styles.total}>Delivery charge</Text>
            <Text style={[styles.total, {fontSize: RFValue(12, height)}]}>
              {' '}
              ₹ {data.delivery_charge}
            </Text>
          </View>
          <View
            style={[
              styles.flexBox,
              {
                backgroundColor: '#f5f5f5',
                paddingHorizontal: 10,
                paddingBottom: 5,
              },
            ]}>
            <Text style={styles.total}>Service charge</Text>
            <Text style={[styles.total, {fontSize: RFValue(12, height)}]}>
              {' '}
              ₹ {data.service_charge}
            </Text>
          </View>
          {data.purchase_point_amount != '0.00' && (
            <View
              style={[
                styles.flexBox,
                {
                  backgroundColor: '#f5f5f5',
                  paddingHorizontal: 10,
                  paddingBottom: 5,
                },
              ]}>
              <Text style={styles.total}>Purchase point</Text>
              <Text style={[styles.total, {fontSize: RFValue(12, height)}]}>
                {' '}
                ₹ {data.purchase_point_amount}
              </Text>
            </View>
          )}

          <View
            style={[
              styles.flexBox,
              {
                backgroundColor: '#f5f5f5',
                paddingHorizontal: 10,
                paddingBottom: 5,
                borderBottomLeftRadius: 10,
                borderBottomRightRadius: 10,
              },
            ]}>
            <Text style={[styles.total, {fontSize: RFValue(13, height)}]}>
              Total amount
            </Text>
            <Text style={[styles.total, {fontSize: RFValue(13, height)}]}>
              {' '}
              ₹ {data.amount_payable}
            </Text>
          </View>
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => {
              props.setModalVisible(false);
            }}
            style={styles.loginButton}>
            <Text style={styles.loginText}>Okay</Text>
          </TouchableOpacity>
        </ScrollView>
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 40,
          }}>
          <ActivityIndicator size={30} color="#85bc3a" />
        </View>
      )}
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  subTextBox: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 10,
    flexWrap: 'wrap',
  },
  subText: {
    fontSize: RFValue(12, height),
    fontFamily: 'Poppins-Regular',
    color: '#a6a6a6',
  },
  contentContainer: {
    width: width,
    paddingHorizontal: 30,
    // paddingVertical: 32,
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 1,
    elevation: 1,
    borderColor: '#eee',
  },
  loginButton: {
    backgroundColor: '#85bc3a',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 35,
    paddingVertical: 10,
  },
  headerText: {
    fontSize: RFValue(15, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: RFValue(14, height),
    fontFamily: 'Poppins-SemiBold',
  },
  loginText: {
    textAlign: 'center',
    fontSize: RFValue(15, height),
    color: '#fff',
    fontFamily: 'Poppins-SemiBold',
  },
  mainHead: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
    color: '#000',
  },
  flexBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  Items: {
    fontFamily: 'Poppins-Regular',
    fontSize: RFValue(13, height),
  },
  dropBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#aaa',
    backgroundColor: '#f5f5f5',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  thirdBox: {
    alignItems: 'center',
    marginBottom: 20,
    marginTop: 35,
  },
  total: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(12, height),
  },
  topContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f57952',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    marginVertical: 15,
    paddingVertical: 15,
    width: width * 0.9,
  },
  idLetter: {
    fontFamily: 'Poppins-Regular',
    fontSize: RFValue(14, height),
    color: '#000',
  },
  idMiddleText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
  },
  idNumber: {
    backgroundColor: '#b3f9aa',
    fontSize: RFValue(14, height),
    fontFamily: 'Poppins-SemiBold',
  },
  locationImageBox: {
    width: 20,
    height: 20,
  },
  locationBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  locationImage: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  shopName: {
    fontSize: RFValue(13, height),
    fontFamily: 'Poppins-SemiBold',
  },
  kmText: {
    color: '#a6a6a6',
    fontFamily: 'Poppins-Regular',
    fontSize: RFValue(12, height),
  },
});
