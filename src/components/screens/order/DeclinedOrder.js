import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  SafeAreaView,
  ImageEditor,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const {height, width} = Dimensions.get('window');
import {useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import DeclineEmptyPage from './DeclineEmptyPage';
import OrderInfoModal from './OrderInfoModal';

import Modal from 'react-native-modal';

export default function DeclinedOrder(props) {
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const [pk, setPk] = useState('');
  useEffect(() => {
    console.warn(props.data);
  });
  return (
    <>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.contentScroll}>
        {props.data.length > 0 ? (
          <View>
            {props.data &&
              props.data.length > 0 &&
              props.data.map((item, index) => (
                <View style={styles.mainContainer} key={index}>
                  <Text style={styles.header}>{item.date}</Text>
                  {item.items &&
                    item.items.length > 0 &&
                    item.items.map((item, index) => (
                      <View key={index} style={styles.OrderBox}>
                        <Text style={styles.id}>
                          Order ID :{' '}
                          <Text style={styles.boldId}>
                            {' '}
                            ORD
                            {item.order_id.slice(item.order_id.length - 4)}
                          </Text>
                        </Text>
                        <TouchableOpacity
                          onPress={() => {
                            setPk(item.order);
                            setModalVisible(true);
                            // navigation.navigate('OrderInfoModal', {
                            //   pk: item.order,
                            // });
                          }}
                          style={styles.imageBox}>
                          <Image
                            style={styles.image}
                            source={require('../../../assets/icons1/round.png')}
                          />
                        </TouchableOpacity>
                      </View>
                    ))}
                </View>
              ))}
          </View>
        ) : (
          <DeclineEmptyPage />
        )}

        <View style={{height: height * 0.1}} />
        <ScrollView>
          <Modal
            onBackButtonPress={() => {
              setModalVisible(false);
            }}
            onBackdropPress={() => {
              setModalVisible(false);
            }}
            isVisible={modalVisible}
            backdropOpacity={0.7}
            style={{
              margin: 0,
              screenBackgroundColor: 'transparent',
              modalPresentationStyle: 'overCurrentContext',
              justifyContent: 'flex-end',
            }}
            propagateSwipe={true}
            useNativeDriver={false}
            onSwipeComplete={() => setModalVisible(false)}
            swipeDirection={['down']}
            animationOut={'fadeOutDown'}
            animationInTiming={500}
            animationOutTiming={500}>
            <OrderInfoModal setModalVisible={setModalVisible} pk={pk} />
          </Modal>
        </ScrollView>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  contentScroll: {minHeight: '100%'},
  OrderBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginVertical: 10,
    paddingVertical: 10,
    backgroundColor: '#f5f5f5',
    borderRadius: 10,
  },
  mainContainer: {
    marginBottom: 20,
    paddingHorizontal: 15,
    paddingVertical: 15,
    backgroundColor: '#fff',
    borderRadius: 10,
  },
  imageBox: {
    width: 20,
    height: 20,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  header: {
    fontSize: RFValue(14, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#f6825e',
  },
  id: {
    fontFamily: 'Poppins-regular',
    fontSize: RFValue(13, height),
  },
  boldId: {
    fontSize: RFValue(13, height),
    fontFamily: 'Poppins-SemiBold',
  },
});
