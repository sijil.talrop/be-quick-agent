import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  TouchableWithoutFeedback,
  Switch,
  SafeAreaView,
} from 'react-native';
var {height, width} = Dimensions.get('window');
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

const DeliveryPopup = ({route}) => {
  const [switchValue, setSwitchValue] = useState(false);
  const toggleSwitch = value => {
    setSwitchValue(value);
  };
  return (
    <>
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={{alignItems: 'center'}}>
            <View style={styles.imageContainer}>
              <Image
                source={require('../../../assets/illustrations/delivered_nill_vector.png')}
                style={styles.image}
              />
            </View>
            <View style={styles.ForgotBox}>
              <Text style={styles.processText}>Oh no,</Text>
              <Text style={styles.subText}>
                You haven't started delivering yet.
              </Text>
            </View>
          </View>
          {/* <TouchableOpacity activeOpacity={0.8} style={styles.bottomImage}>
            <Text style={styles.buttonFont}>Start Delivering</Text>
          </TouchableOpacity> */}
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    // backgroundColor: '#fff',
    alignItems: 'center',
    minHeight: '100%',
    // width: width,
    height: height * 0.5,
    // borderWidth: 1,
    justifyContent: 'center',
  },
  imageContainer: {
    width: width * 0.25,
    height: width * 0.25,
  },
  processText: {
    color: '#000',
    fontFamily: 'Poppins-SemiBold',
    textAlign: 'center',
    fontSize: RFValue(14, height),
  },
  subText: {
    fontFamily: 'Poppins-Regular',
    color: '#737373',
    // fontSize: 15,
    fontSize: RFValue(13, height),
  },
  ForgotBox: {
    alignItems: 'center',
    marginVertical: height * 0.05,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  bottomImage: {
    borderWidth: 0.1,
    borderRadius: 10,
    borderColor: '#eee',
    elevation: 0.7,
    backgroundColor: '#85bc3a',
    paddingVertical: 10,
    alignItems: 'center',
  },
  buttonFont: {
    fontFamily: 'Poppins-SemiBold',
    color: '#fff',
  },
});

export default DeliveryPopup;
