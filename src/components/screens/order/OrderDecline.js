import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Animated,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
const {width, height} = Dimensions.get('window');

export default function OrderDecline(props) {
  return (
    <Animated.View style={[styles.contentContainer]}>
      <View>
        <View style={{alignItems: 'center', marginBottom: 20}}>
          <Text style={styles.headerText}>
            Are you sure to decline the order?
          </Text>
          <Text style={styles.SubText}>
            it may affect negatively on your ratings
          </Text>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              props.setModalVisible(false);
              props.setReason_modal(true);
            }}
            style={[styles.loginButton, {backgroundColor: '#85bc3a'}]}>
            <Text style={styles.loginText}>Yes</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              props.setModalVisible(false);
            }}
            style={[styles.loginButton, {backgroundColor: '#d86666'}]}>
            <Text style={styles.loginText}>No</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  contentContainer: {
    width: width,
    paddingHorizontal: 30,
    paddingVertical: '10%',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 1,
    borderColor: '#eee',
  },
  loginButton: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    paddingHorizontal: 15,
    width: width * 0.4,
    borderRadius: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    fontSize: RFValue(14, height),
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
  },
  SubText: {
    fontFamily: 'Poppins-LightItalic',
    color: '#505050',
    fontSize: RFValue(13, height),
  },
  loginText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(15, height),
    color: '#fff',
  },
});
