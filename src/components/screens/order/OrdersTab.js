import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  StatusBar,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import {BASE_URL} from '../../../../Settings';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BasicHeading from '../heading/BasicHeading';
import DeliveredOrder from './DeliveredOrder';
import CancelledOrder from './CancelledOrder';
import DeclinedOrder from './DeclinedOrder';
import {useNavigation} from '@react-navigation/native';

const {height, width} = Dimensions.get('window');

export default function OrdersTab(props) {
  const navigation = useNavigation();
  const [index, setIndex] = React.useState(0);
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selectedBox, setSelectedBox] = useState('delivered');
  const confirmHandler = value => {
    setSelectedBox(value);
  };

  useEffect(() => {
    getOrderStatus();
  }, [selectedBox]);

  const getOrderStatus = async () => {
    setLoading(true);
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let get_url = BASE_URL + `orders/agent-order-list/?status=${selectedBox}`;
    console.log(get_url);
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then(response => {
        if (response.data.StatusCode === 6000) {
          setTimeout(() => {
            console.log(response.data.data.serialized, selectedBox);
            setData(response.data.data.serialized);
            setLoading(false);
          }, 500);
        } else {
          setTimeout(() => {
            console.log('else', selectedBox);
            setData([]);
            setLoading(false);
          }, 500);
        }
      })
      .catch(error => {
        setTimeout(() => {
          console.log(error);
          setLoading(false);
        }, 500);
      });
  };

  const Tab = [
    {
      Box: 'Delivered',
      value: 'delivered',
    },
    {
      Box: 'Cancelled',
      value: 'cancelled',
    },
    {
      Box: 'Declined',
      value: 'declined',
    },
  ];
  const renderTabList = () => {
    if (selectedBox == 'delivered') {
      return <DeliveredOrder data={data} />;
    } else if (selectedBox == 'cancelled') {
      return <CancelledOrder data={data} />;
    } else if (selectedBox == 'declined') {
      return <DeclinedOrder data={data} />;
    }
  };

  return (
    <>
      <SafeAreaView>
        <ImageBackground
          source={require('../../../assets/images/Group20260.png')}
          style={styles.backgroundImage}></ImageBackground>
        <View style={styles.topView}>
          <View style={styles.iconBox}>
            <BasicHeading title="Orders" />
          </View>
          <View style={styles.tabView}>
            {Tab.map((item, index) => (
              <TouchableOpacity
                onPress={() => {
                  confirmHandler(item.value);
                }}
                style={[
                  styles.tabBox,
                  {
                    backgroundColor:
                      item.value === selectedBox && selectedBox == 'delivered'
                        ? '#85bc3a'
                        : item.value === selectedBox &&
                          selectedBox != 'delivered'
                        ? '#f6825e'
                        : 'transparent',
                  },
                ]}>
                <Text
                  style={[
                    styles.buttonText,
                    {color: item.value == selectedBox ? '#fff' : '#aaa'},
                  ]}>
                  {item.Box}
                </Text>
              </TouchableOpacity>
            ))}
          </View>
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={styles.contentScroll}>
            {!loading && renderTabList()}
            {loading && (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ActivityIndicator size={30} color="#85bc3a" />
              </View>
            )}
          </ScrollView>
        </View>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  topView: {
    // backgroundColor: '#f5f5f5',
    justifyContent: 'space-around',
    // alignItems: 'center',
    paddingHorizontal: 20,
    minHeight: '100%',
  },
  backgroundImage: {
    flex: 1,
    // justifyContent: 'flex-end',
    resizeMode: 'cover',
    // justifyContent: 'flex-end',
    width: width * 0.4,
    height: height * 0.3,
    position: 'absolute',
    right: 0,
    // zIndex: 1,
  },
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // backgroundColor: '#fff',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    marginVertical: 20,
  },
  tabBox: {
    backgroundColor: 'red',
    width: width * 0.3,
    alignItems: 'center',
    borderRadius: 10,
    paddingVertical: 10,
  },
  iconBox: {
    paddingTop: 20,
    width: width - 30,
  },
  buttonText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  contentScroll: {
    paddingTop: 10,
  },
});
