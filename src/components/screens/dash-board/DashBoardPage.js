import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  ActivityIndicator,
} from 'react-native';
const {height, width} = Dimensions.get('window');
import {BASE_URL} from '../../../../Settings';
import BasicHeading from '../heading/BasicHeading';

import {Rating, AirbnbRating} from 'react-native-ratings';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

import ActiveHours from '../../../assets/icons2/active-hours.svg';
import DistanceCovered from '../../../assets/icons2/distance-covered.svg';
import OrdersDelivered from '../../../assets/icons2/orders-delivered.svg';
import OrdersCancelled from '../../../assets/icons2/orders-cancelled.svg';
import OrdersDeclined from '../../../assets/icons2/orders-declined.svg';
import CashOnHand from '../../../assets/icons2/cash-on-hand.svg';
import RateStar from '../../../assets/icons2/rate-star.svg';
import PieChart from '../../../assets/icons2/pie-chart.svg';

export default function DashBoardPage() {
  const [loading, setLoading] = useState(true);
  const [rating, setRating] = useState(4);
  const [is_visible, setIs_visible] = useState(false);
  const [is_show, setIs_show] = useState(false);
  const [today_activity, setToday_activity] = useState({});
  const [weakly_activity, setWeakly_activity] = useState({});
  const [details, setDetails] = useState({});
  const [button_change, setButton_change] = useState(false);

  useEffect(() => {
    getActivity();
  }, []);

  const getActivity = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let get_url = BASE_URL + 'delivery-agents/dashboard/';
    console.log(get_url);
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          console.log(data.todays_activity);
          setTimeout(() => {
            setDetails(data);
            console.log(data);
            setToday_activity(data.todays_activity);
            setWeakly_activity(data.weakly_activity);
            setLoading(false);
          }, 500);
        } else {
          setTimeout(() => {
            setDetails({});
            setToday_activity({});
            setWeakly_activity({});
            setLoading(false);
          }, 500);
        }
      })
      .catch(error => {
        setTimeout(() => {
          console.log(error.response);
          setDetails({});
          setToday_activity({});
          setWeakly_activity({});
          setLoading(false);
        }, 500);
      });
  };

  const handoverCash = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let post_url = BASE_URL + 'delivery-agents/cash-handover/';
    console.warn(post_url, details.hand_over_cash);
    axios
      .post(
        post_url,
        {
          amount: details.hand_over_cash,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          setButton_change(true);
        } else {
          console.log(response.data);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const ratingRender = () => {
    return (
      <View style={{marginBottom: 15}}>
        <View style={styles.topContainer}>
          <View style={styles.imageBox}>
            {/* <Image
              style={styles.image}
              source={require('../../../assets/icons2/star1.png')}
            /> */}
            <RateStar width={25} height={25} />
          </View>
          <Text style={styles.head}>My Rating</Text>
        </View>
        <View
          style={{alignItems: 'center', backgroundColor: '#fff', padding: 10}}>
          <Rating
            startingValue={details.rating}
            readonly={true}
            type="custom"
            tintColor="#fff"
            ratingColor="#85bc3a"
            ratingBackgroundColor="#efefef"
            ratingCount={5}
            imageSize={30}
            style={{paddingVertical: 10}}
            isDisabled={false}
          />
          {details.Rating > 0 ? (
            <Text style={styles.ratingText}>
              {details.rating} (Rated by {details.total_customer_rated} BeQuick
              Customers)
            </Text>
          ) : (
            <Text style={styles.ratingText}>
              {details.rating} (No customers rated yet)
            </Text>
          )}
        </View>
      </View>
    );
  };

  const activeActivity = [
    {
      active: 'Active Hours',
      detail: '10 hr',
      image: require('../../../assets/icons2/active_hours.png'),
    },
    {
      active: 'Distance Covered',
      detail: '35 km',
      image: require('../../../assets/icons2/distance_covered.png'),
    },
    {
      active: 'Orders Delivered',
      detail: '25 Orders',
      image: require('../../../assets/icons2/orders_delivered-1.png'),
    },
    {
      active: 'Orders Cancelled',
      detail: 'None',
      image: require('../../../assets/icons2/Orders_Cancelled-1.png'),
    },
    {
      active: 'Orders Declined',
      detail: 'None',
      image: require('../../../assets/icons2/orders_declined-1.png'),
    },
    {
      active: 'Cash on Hand',
      detail: '2450',
      image: require('../../../assets/icons2/cash_on_hand-1.png'),
    },
  ];

  const weeklyActivity = () => {
    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            setIs_show(!is_show);
          }}
          style={styles.topContainer}>
          <View style={styles.imageBox}>
            {/* <Image
              style={styles.image}
              source={require('../../../assets/icons2/activity.png')}
            /> */}
            <PieChart width={25} height={25} />
          </View>
          <Text style={styles.head}>Weakly Activity</Text>
        </TouchableOpacity>
        {is_show && (
          <View style={{paddingTop: 10}}>
            <View style={styles.flexBox}>
              <View style={styles.middleContent}>
                <View style={styles.activeImage}>
                  {/* <Image
                    style={styles.image}
                    source={require('../../../assets/icons2/distance_covered.png')}
                  /> */}
                  <DistanceCovered width={25} height={25} />
                </View>
                <Text style={styles.content}>Distance Covered</Text>
              </View>
              <Text style={styles.detailText}>
                {weakly_activity.distance_covered} km
              </Text>
            </View>
            <View style={styles.flexBox}>
              <View style={styles.middleContent}>
                <View style={styles.activeImage}>
                  {/* <Image
                    style={styles.image}
                    source={require('../../../assets/icons2/orders_delivered-1.png')}
                  /> */}
                  <OrdersDelivered width={25} height={25} />
                </View>
                <Text style={styles.content}>Orders Delivered</Text>
              </View>
              <Text style={styles.detailText}>
                {weakly_activity.orders_delivered} Orders
              </Text>
            </View>
            <View style={styles.flexBox}>
              <View style={styles.middleContent}>
                <View style={styles.activeImage}>
                  {/* <Image
                    style={styles.image}
                    source={require('../../../assets/icons2/Orders_Cancelled-1.png')}
                  /> */}
                  <OrdersCancelled width={25} height={25} />
                </View>
                <Text style={styles.content}>Orders Cancelled</Text>
              </View>
              <Text style={styles.detailText}>
                {weakly_activity.orders_cancelled} Orders
              </Text>
            </View>
            <View style={styles.flexBox}>
              <View style={styles.middleContent}>
                <View style={styles.activeImage}>
                  {/* <Image
                    style={styles.image}
                    source={require('../../../assets/icons2/orders_declined-1.png')}
                  /> */}
                  <OrdersDeclined width={25} height={25} />
                </View>
                <Text style={styles.content}>Orders Declined</Text>
              </View>
              <Text style={styles.detailText}>
                {weakly_activity.orders_declined} Order
              </Text>
            </View>
            <View style={styles.flexBox}>
              <View style={styles.middleContent}>
                <View style={styles.activeImage}>
                  {/* <Image
                    style={styles.image}
                    source={require('../../../assets/icons2/cash_on_hand-1.png')}
                  /> */}
                  <CashOnHand width={25} height={25} />
                </View>
                <Text style={styles.content}>Cash on Hand</Text>
              </View>
              <Text style={styles.detailText}>
                ₹{weakly_activity.cash_on_hands}
              </Text>
            </View>
          </View>
        )}
        {details.is_handover && (
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              if (!button_change) {
                handoverCash();
              }
            }}
            style={{marginTop: 15}}>
            {button_change ? (
              <View style={styles.firstButton}>
                <Icon name="shield-check-outline" size={20} color="#fff" />
                <Text style={styles.FirstText}>
                  You will notified after verification
                </Text>
              </View>
            ) : (
              <View style={styles.loginButton}>
                <Text style={styles.loginText}>Handover Cash</Text>
                <View style={styles.buttonImageContent}>
                  <Text
                    style={[
                      styles.loginText,
                      {marginRight: 20, fontSize: RFValue(18, height)},
                    ]}>
                    ₹ {details.hand_over_cash}
                  </Text>
                  <Image
                    source={require('../../../assets/icons1/handover_cash.png')}
                  />
                </View>
              </View>
            )}
          </TouchableOpacity>
        )}
      </View>
    );
  };

  const todayActivity = () => {
    return (
      <View style={{marginBottom: 15, paddingTop: 10}}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            setIs_visible(!is_visible);
          }}
          style={styles.topContainer}>
          <View style={styles.imageBox}>
            {/* <Image
              style={styles.image}
              source={require('../../../assets/icons2/activity.png')}
            /> */}
            <PieChart width={25} height={25} />
          </View>
          <Text style={styles.head}>Today's Activity</Text>
        </TouchableOpacity>
        {is_visible && (
          <View style={{paddingTop: 10}}>
            <View style={styles.flexBox}>
              <View style={styles.middleContent}>
                <View style={styles.activeImage}>
                  {/* <Image
                    style={styles.image}
                    source={require('../../../assets/icons2/active_hours.png')}
                  /> */}
                  <ActiveHours width={25} height={25} />
                </View>
                <Text style={styles.content}>Active Hours</Text>
              </View>
              <Text style={styles.detailText}>
                {today_activity.active_hours} hr
              </Text>
            </View>

            <View style={styles.flexBox}>
              <View style={styles.middleContent}>
                <View style={styles.activeImage}>
                  {/* <Image
                    style={styles.image}
                    source={require('../../../assets/icons2/distance_covered.png')}
                  /> */}
                  <DistanceCovered width={25} height={25} />
                </View>
                <Text style={styles.content}>Distance Covered</Text>
              </View>
              <Text style={styles.detailText}>
                {today_activity.distance_covered} km
              </Text>
            </View>

            <View style={styles.flexBox}>
              <View style={styles.middleContent}>
                <View style={styles.activeImage}>
                  {/* <Image
                    style={styles.image}
                    source={require('../../../assets/icons2/orders_delivered-1.png')}
                  /> */}
                  <OrdersDelivered width={25} height={25} />
                </View>
                <Text style={styles.content}>Orders Delivered</Text>
              </View>
              <Text style={styles.detailText}>
                {today_activity.orders_delivered} Orders
              </Text>
            </View>

            <View style={styles.flexBox}>
              <View style={styles.middleContent}>
                <View style={styles.activeImage}>
                  {/* <Image
                    style={styles.image}
                    source={require('../../../assets/icons2/Orders_Cancelled-1.png')}
                  /> */}
                  <OrdersCancelled width={25} height={25} />
                </View>
                <Text style={styles.content}>Orders Cancelled</Text>
              </View>
              <Text style={styles.detailText}>
                {today_activity.orders_cancelled} Orders
              </Text>
            </View>

            <View style={styles.flexBox}>
              <View style={styles.middleContent}>
                <View style={styles.activeImage}>
                  {/* <Image
                    style={styles.image}
                    source={require('../../../assets/icons2/orders_declined-1.png')}
                  /> */}
                  <OrdersDeclined width={25} height={25} />
                </View>
                <Text style={styles.content}>Orders Declined</Text>
              </View>
              <Text style={styles.detailText}>
                {today_activity.orders_declined} Orders
              </Text>
            </View>

            <View style={styles.flexBox}>
              <View style={styles.middleContent}>
                <View style={styles.activeImage}>
                  {/* <Image
                    style={styles.image}
                    source={require('../../../assets/icons2/cash_on_hand-1.png')}
                  /> */}
                  <CashOnHand width={25} height={25} />
                </View>
                <Text style={styles.content}>Cash on Hand</Text>
              </View>
              <Text style={styles.detailText}>
                ₹{today_activity.cash_on_hands}
              </Text>
            </View>
          </View>
        )}
      </View>
    );
  };
  return (
    <View>
      <ImageBackground
        source={require('../../../assets/images/Group20260.png')}
        style={styles.backgroundImage}></ImageBackground>
      <ScrollView
        style={{
          paddingHorizontal: 20,
          minHeight: '100%',
          // backgroundColor: '#f',
        }}>
        {!loading ? (
          <View
            style={{
              paddingVertical: 50,
            }}>
            <View style={{marginBottom: 20}}>
              <BasicHeading title="Dashboard" />
            </View>
            {ratingRender()}
            {todayActivity()}
            {weeklyActivity()}
          </View>
        ) : (
          <View
            style={{
              height: height,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator size={30} color="#85bc3a" />
          </View>
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  topContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f57952',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    // marginVertical: 15,
    paddingVertical: 10,
  },
  imageBox: {
    width: 25,
    height: 25,
    marginRight: 10,
  },
  activeImage: {
    width: 30,
    height: 30,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  head: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
    color: '#fff',
  },
  ratingText: {
    fontFamily: 'Poppins-Regular',
    fontSize: RFValue(13, height),
    color: '#817c7c',
  },
  middleContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  flexBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
    paddingHorizontal: 20,
    paddingTop: 10,
  },
  content: {
    fontFamily: 'Poppins-Regular',
    fontSize: RFValue(13, height),
    marginLeft: 20,
  },
  detailText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  loginButton: {
    backgroundColor: '#85bc3a',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  loginText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
    color: '#fff',
  },
  buttonImageContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  firstButton: {
    flexDirection: 'row',
    backgroundColor: '#404040',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
  },
  FirstText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
    color: '#fff',
    marginLeft: 20,
  },
  backgroundImage: {
    flex: 1,
    // justifyContent: 'flex-end',
    resizeMode: 'cover',
    // justifyContent: 'flex-end',
    width: width * 0.4,
    height: height * 0.3,
    position: 'absolute',
    right: 0,
    zIndex: -1,
  },
});
