import React, {useState, useEffect, useRef} from 'react';
const {width, height} = Dimensions.get('window');

import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  ScrollView,
  SafeAreaView,
  ImageBackground,
  ActivityIndicator,
} from 'react-native';
import {BASE_URL} from '../../../../Settings';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BasicHeading from '../../screens/heading/BasicHeading';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
// import {SafeAreaView} from 'react-native-safe-area-context';
import axios from 'axios';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
const Reward_Box_Height = width * 0.3;
const card_width = width * 0.9;

const NotificationPage = () => {
  const navigation = useNavigation();
  const [like, setLike] = useState(true);
  const [loading, setLoading] = useState(true);
  const [unreadNotification, setUnreadNotification] = useState([]);
  const [readNotification, setReadNotification] = useState([]);
  useEffect(() => {
    getNotifications();
  }, []);

  const getNotifications = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let get_url = BASE_URL + 'delivery-agents/notifications/';
    console.warn(get_url, access_token);
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          setTimeout(() => {
            console.log(data, '////');
            setUnreadNotification(data.unread_notifications);
            setReadNotification(data.read_notifications);
            setLoading(false);
          }, 500);
        } else {
          setTimeout(() => {
            console.log('no data');
            setLoading(false);
          }, 500);
        }
      })
      .catch(error => {
        console.log(error.response, '////');
      });
  };

  const Unread = () => {
    if (unreadNotification && unreadNotification.length > 0) {
      return unreadNotification.map((item, index) => (
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('OnDutyPage');
          }}
          key={index}
          activeOpacity={0.9}
          style={styles.mainContainer}>
          <View
            style={[
              styles.cartCard,
              {
                borderBottomWidth:
                  unreadNotification.length === unreadNotification.index - 1
                    ? 0.7
                    : 0,
              },
            ]}>
            <View style={styles.imageBox}>
              <Text style={styles.mainDetail}>{item.short_name}</Text>
            </View>
            <View style={[styles.MonthStatus, {marginLeft: 10}]}>
              <Text style={styles.Content}>{item.notification}</Text>
              <Text style={styles.DateFont}>{item.time}</Text>
            </View>
          </View>
        </TouchableOpacity>
      ));
    } else {
      return <Text style={styles.DateFont}>No unread notifications</Text>;
    }
  };

  const Read = () => {
    if (readNotification && readNotification.length > 0) {
      return readNotification.map((item, index) => (
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('OnDutyPage');
          }}
          key={index}
          activeOpacity={0.9}
          style={styles.mainContainer}>
          <View
            style={[
              styles.cartCard,
              {
                borderBottomWidth:
                  readNotification.length != index + 1 ? 0.7 : 0,
              },
            ]}>
            <View style={[styles.imageBox, {backgroundColor: '#9c9c9c'}]}>
              <Text style={styles.mainDetail}>{item.short_name}</Text>
            </View>
            <View style={[styles.MonthStatus, {marginLeft: 10}]}>
              <Text style={styles.Content}>{item.notification}</Text>
              <Text style={styles.DateFont}>{item.time}</Text>
            </View>
          </View>
        </TouchableOpacity>
      ));
    } else {
      return <Text style={styles.DateFont}>No read notifications</Text>;
    }
  };

  return (
    <>
      <View>
        <ImageBackground
          source={require('../../../assets/images/Group20260.png')}
          style={styles.backgroundImage}></ImageBackground>
        <View style={styles.headContainer}>
          <BasicHeading title="Notification" />
        </View>
        {!loading ? (
          <ScrollView
            contentContainerStyle={{minHeight: '100%', paddingHorizontal: 15}}>
            <View style={styles.contentBox}>
              <Text style={styles.Head}>Unread</Text>
              {Unread()}
            </View>
            <View style={[styles.contentBox, {marginTop: 15}]}>
              <Text style={styles.Head}>read</Text>
              {Read()}
            </View>
          </ScrollView>
        ) : (
          <View
            style={{
              height: height * 0.9,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator size={40} color="#85bc3a" />
          </View>
        )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    // backgroundColor: '#fff',
    marginVertical: 15,
    borderRadius: 10,
  },
  backgroundImage: {
    flex: 1,
    // justifyContent: 'flex-end',
    resizeMode: 'cover',
    // justifyContent: 'flex-end',
    width: width * 0.4,
    height: height * 0.3,
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: -1,
  },
  contentBox: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: '#fff',
    borderRadius: 10,
  },
  cartCard: {
    width: card_width,
    // backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#aaa',
    paddingVertical: 10,
  },
  imageBox: {
    width: 40,
    height: 40,
    overflow: 'hidden',
    borderRadius: 30,
    backgroundColor: '#f57952',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Head: {
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
    fontSize: RFValue(14, height),
  },
  headContainer: {
    paddingHorizontal: 15,
    paddingVertical: 20,
    // backgroundColor: '#fff',
  },
  MonthStatus: {},
  DateFont: {
    color: '#9c9c9c',
    fontSize: RFValue(12, height),
    fontFamily: 'Poppins-Regular',
  },
  Content: {
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
    fontSize: RFValue(12, height),
  },
  mainDetail: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(14, height),
    color: '#fff',
  },
});

export default NotificationPage;
