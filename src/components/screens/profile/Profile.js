import React, {useRef, useEffect, useState, useContext} from 'react';
import {
  StyleSheet,
  View,
  Animated,
  Dimensions,
  Text,
  Image,
  TouchableOpacity,
  Alert,
  ImageBackground,
  ActivityIndicator,
} from 'react-native';
import BasicHeading from '../heading/BasicHeading';
const {width, height} = Dimensions.get('window');
import {BASE_URL} from '../../../../Settings';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import axios from 'axios';
import Icon from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {UserContext} from '../../../contexts/stores/UserStore';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

export default function Profile() {
  const {userState, userDispatch} = useContext(UserContext);
  const [profile, setProfile] = useState({});
  const [loading, setLoading] = useState(true);

  const [image_data, setImageData] = useState('');
  const [image_response, setImageResponse] = useState('');

  useEffect(() => {
    getProfile();
  }, []);

  useEffect(() => {
    if (image_data) {
      updateProfile();
    }
  }, [image_data]);

  const logOut = () => {
    Alert.alert('Logout', 'Are you sure want to logout', [
      {
        text: 'No',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'Yes',
        onPress: () => {
          _signOutAsync();
        },
      },
    ]);
  };

  const _signOutAsync = async () => {
    console.warn('Clear Async');
    setTimeout(() => {
      userDispatch({
        type: 'UPDATE_USER',
        user: {
          user: 'not user',
        },
      });
    }, 500);
    await AsyncStorage.clear();
  };

  const uploadImage = async () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
      },
      response => {
        console.log();
        if (!response['didCancel']) {
          console.log(response);
          setImageResponse(response.uri);
          setImageData({
            uri: response.uri,
            name: response.fileName,
            filename: response.fileName,
            type: 'image/png',
          });
          console.log(response.uri);
        }
      },
    );
  };

  const updateProfile = async () => {
    setLoading(true);
    let post_url = BASE_URL + 'delivery-agents/edit-image/';
    console.log(post_url);
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let formData = new FormData();
    formData.append('image', image_data);
    axios
      .post(post_url, formData, {
        headers: {
          Authorization: 'Bearer ' + access_token,
          'Content-Type': 'multipart/form-data; ',
        },
      })
      .then(response => {
        let {StatusCode} = response.data;
        if (StatusCode == 6000) {
          setTimeout(() => {
            getProfile();
            setLoading(false);
            setImageData('');
          }, 500);
        } else {
          setTimeout(() => {
            setLoading(false);
            console.log(response.data, 'else');
            setImageData('');
          }, 500);
        }
      })
      .catch(error => {
        setLoading(false);
        console.log(error.response, 'catch');
        setImageData('');
      });
  };

  const getProfile = async () => {
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let get_url = BASE_URL + 'delivery-agents/profile/';
    console.log(get_url);
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then(response => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          setTimeout(() => {
            setProfile(data);
            console.log(data);
            setLoading(false);
          }, 500);
        } else {
          console.log('no data');
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../../../assets/images/Group20260.png')}
        style={styles.backgroundImage}></ImageBackground>
      {!loading ? (
        <View style={styles.mainContentBox}>
          <View>
            <BasicHeading title="My Profile" />
          </View>
          <View style={{alignItems: 'center'}}>
            <View style={styles.imageBox}>
              {!profile.profile_image ? (
                <View style={styles.withoutImage}>
                  <Text
                    style={{
                      color: '#fff',
                      fontSize: RFValue(25, height),
                      fontFamily: 'Poppins-SemiBold',
                      textTransform: 'capitalize',
                    }}>
                    {profile.username.charAt(0)}
                  </Text>
                  <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {
                      uploadImage();
                    }}
                    style={{
                      backgroundColor: '#fff',
                      padding: 10,
                      borderRadius: 20,
                      position: 'absolute',
                      right: 3,
                      bottom: 10,
                      elevation: 5,
                      zIndex: 15,
                    }}>
                    <Icon name={'edit'} size={20} color={'#9c9c9c'} />
                  </TouchableOpacity>
                </View>
              ) : (
                <View style={styles.withImage}>
                  <Image
                    style={styles.image2}
                    resizeMode={'cover'}
                    source={{uri: profile.profile_image}}
                  />
                  <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {
                      uploadImage();
                    }}
                    style={{
                      backgroundColor: '#fff',
                      padding: 10,
                      borderRadius: 20,
                      position: 'absolute',
                      right: 3,
                      bottom: 10,
                      elevation: 5,
                      zIndex: 15,
                    }}>
                    <Icon name={'edit'} size={20} color={'#9c9c9c'} />
                  </TouchableOpacity>
                </View>
              )}
            </View>
          </View>
          <View style={styles.main}>
            <View style={styles.addressContent}>
              <Text style={styles.leftText}>{profile.staff_id}</Text>
              <Text style={styles.rightText}>Agent ID</Text>
            </View>
            <View style={styles.addressContent}>
              <Text style={styles.leftText}>{profile.username}</Text>
              <Text style={styles.rightText}>Username</Text>
            </View>
            <View style={styles.addressContent}>
              <Text style={styles.leftText}>{profile.phone}</Text>
              <Text style={styles.rightText}>Phone</Text>
            </View>
            <View style={styles.addressContent}>
              <Text style={styles.leftText}>{profile.email}</Text>
              <Text style={styles.rightText}>Email ID</Text>
            </View>
            <View style={styles.addressContent}>
              <Text style={styles.leftText}>22</Text>
              <Text style={styles.rightText}>Age</Text>
            </View>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => {
                logOut();
              }}
              activeOpacity={0.7}
              style={styles.button}>
              <View style={styles.logOut}>
                <Image
                  style={styles.image}
                  source={require('../../../assets/icons1/logout.png')}
                />
              </View>
              <Text style={styles.logOutText}>Logout</Text>
            </TouchableOpacity>
          </View>
        </View>
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size={30} color="#85bc3a" />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    minHeight: '100%',
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
    backgroundColor: '#fff',
  },
  backgroundImage: {
    flex: 1,
    // justifyContent: 'flex-end',
    resizeMode: 'cover',
    // justifyContent: 'flex-end',
    width: width * 0.4,
    height: height * 0.3,
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 5,
  },
  mainSwitchContainer: {},
  mainContentBox: {
    paddingHorizontal: 20,
    justifyContent: 'space-around',
    flexDirection: 'column',
    flex: 1,
  },
  main: {},
  addressContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  imageBox: {
    width: width * 0.3,
    height: width * 0.3,
  },
  logOut: {
    width: 25,
    height: 25,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  image2: {
    width: null,
    height: null,
    flex: 1,
    borderRadius: 100,
    resizeMode: 'contain',
  },
  withoutImage: {
    alignItems: 'center',
    justifyContent: 'center',
    width: null,
    height: null,
    flex: 1,
    borderRadius: 100,
    overflow: 'hidden',
    backgroundColor: '#9c9c9c',
  },
  withImage: {
    width: width * 0.3,
    height: width * 0.3,
    overflow: 'hidden',
    borderRadius: 100,
  },
  leftText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(13, height),
  },
  rightText: {
    fontFamily: 'Poppins-LightItalic',
    fontSize: RFValue(13, height),
    color: '#a8a8a8',
  },
  button: {
    backgroundColor: '#85bc3a',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
    borderRadius: 15,
  },
  logOutText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: RFValue(15, height),
    color: '#fff',
    marginLeft: 20,
  },
});
