import React, {createContext, useReducer} from 'react';
import UserReducer from '../reducers/UserReducer';

const initialState = {
  user: '',
  username: '',
  password: '',
  is_duty: false,
  live_time: '',
  device_id: '',
};

const UserStore = ({children}) => {
  const [userState, userDispatch] = useReducer(UserReducer, initialState);

  return (
    <UserContext.Provider value={{userState, userDispatch}}>
      {children}
    </UserContext.Provider>
  );
};

export const UserContext = createContext(initialState);

export default UserStore;
